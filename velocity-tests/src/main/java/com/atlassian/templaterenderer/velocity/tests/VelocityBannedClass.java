package com.atlassian.templaterenderer.velocity.tests;

public class VelocityBannedClass {
    public String getText() {
        return "This should not be rendered and printed from velocity file";
    }
}
