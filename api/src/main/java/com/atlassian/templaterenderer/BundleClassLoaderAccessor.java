package com.atlassian.templaterenderer;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;

import org.apache.commons.collections.iterators.IteratorEnumeration;
import org.osgi.framework.Bundle;

import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;
import com.atlassian.plugin.util.resource.AlternativeResourceLoader;
import com.atlassian.plugin.util.resource.NoOpAlternativeResourceLoader;

import static java.util.Objects.requireNonNull;

/**
 * Utility methods for accessing a bundle as if it was a classloader. Copied from atlassian-plugins-osgi, as
 * {@link com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor} this class is package private there. Which is used
 * by {@code com.atlassian.plugin.osgi.factory.OsgiPluginHelper}.
 * <p>
 *     The returned ClassLoader will have a null parent, so we don't leak classes from main ClassLoader.
 * </p>
 * @see <a href="https://ecosystem.atlassian.net/browse/ATR-27">ATR-27</a>
 * @deprecated Remove in 7.0 - Internally move to the one from Atlassian Plugins, external consumers don't need this.
 */
@Deprecated
public class BundleClassLoaderAccessor {
    public static ClassLoader getClassLoader(final Bundle bundle) {
        return new BundleClassLoader(bundle, new AlternativeDirectoryResourceLoader());
    }

    /// CLOVER:OFF

    /**
     * Fake classloader that delegates to a bundle
     */
    private static class BundleClassLoader extends ClassLoader {
        private final Bundle bundle;
        private final AlternativeResourceLoader altResourceLoader;

        public BundleClassLoader(final Bundle bundle, AlternativeResourceLoader altResourceLoader) {
            // ATR-27: Ensure the parent ClassLoader is null so we don't leak classes from main ClassLoader
            super(null);
            requireNonNull(bundle, "The bundle must not be null");
            if (altResourceLoader == null) {
                altResourceLoader = new NoOpAlternativeResourceLoader();
            }
            this.altResourceLoader = altResourceLoader;
            this.bundle = bundle;
        }

        @Override
        public Class<?> findClass(final String name) throws ClassNotFoundException {
            return bundle.loadClass(name);
        }

        @SuppressWarnings("unchecked")
        @Override
        public Enumeration<URL> findResources(final String name) throws IOException {
            Enumeration<URL> e = bundle.getResources(name);

            // For some reason, getResources() sometimes returns nothing, yet getResource() will return one.  This code
            // handles that strange case
            if (e != null && !e.hasMoreElements()) {
                final URL resource = findResource(name);
                if (resource != null) {
                    e = new IteratorEnumeration(Arrays.asList(resource).iterator());
                }
            }
            return e;
        }

        @Override
        public URL findResource(final String name) {
            URL url = altResourceLoader.getResource(name);
            if (url == null) {
                url = bundle.getResource(name);
            }
            return url;
        }
    }
}
