package com.atlassian.templaterenderer.plugins;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.DisposableBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.tracker.DefaultPluginModuleTracker;
import com.atlassian.plugin.tracker.PluginModuleTracker;
import com.atlassian.templaterenderer.TemplateContextFactory;

/**
 * Implementation of the template context factory
 */
public class TemplateContextFactoryImpl implements TemplateContextFactory, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(TemplateContextFactoryImpl.class);
    private final PluginModuleTracker<Object, TemplateContextItemModuleDescriptor> templateContextItemTracker;

    public TemplateContextFactoryImpl(PluginAccessor pluginAccessor, PluginEventManager eventManager) {
        this.templateContextItemTracker = new DefaultPluginModuleTracker<>(
                pluginAccessor, eventManager, TemplateContextItemModuleDescriptor.class);
    }

    /**
     * Create a context for a template renderer
     *
     * @param contextParams Any extra context parameters that should be added to the context
     * @return A map of the context
     */
    @Override
    public Map<String, Object> createContext(String pluginKey, Map<String, Object> contextParams) {
        Map<String, Object> context = new HashMap<>();
        context.put("context", context);

        for (TemplateContextItemModuleDescriptor desc : templateContextItemTracker.getModuleDescriptors()) {
            if (desc.isGlobal() || desc.getPluginKey().equals(pluginKey)) {
                try {
                    context.put(desc.getContextKey(), desc.getModule());
                } catch (RuntimeException re) {
                    log.error("Error loading module for {}:{}", desc.getPluginKey(), desc.getKey(), re);
                }
            }
        }
        context.putAll(contextParams);
        return context;
    }

    @Override
    public void destroy() {
        templateContextItemTracker.close();
    }
}
