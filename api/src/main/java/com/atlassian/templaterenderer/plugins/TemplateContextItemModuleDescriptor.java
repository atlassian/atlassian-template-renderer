package com.atlassian.templaterenderer.plugins;

import java.util.function.Supplier;

import org.springframework.context.ApplicationContext;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;

/**
 * Module descriptor for template context items.  These may either be a class that gets instantiated on each lookup, or
 * they may reference a component using component-ref.
 */
public class TemplateContextItemModuleDescriptor extends AbstractModuleDescriptor<Object> implements StateAware {
    private Logger log = LoggerFactory.getLogger(TemplateContextItemModuleDescriptor.class);
    private boolean global = false;
    private String contextKey;
    private String componentRef = null;
    private Object component = null;

    private Supplier<Object> moduleSupplier;

    public TemplateContextItemModuleDescriptor() {
        super(ModuleFactory.LEGACY_MODULE_FACTORY);
    }

    @Override
    public void init(Plugin plugin, Element element) throws PluginParseException {
        super.init(plugin, element);
        String globalStr = element.attributeValue("global");
        if (globalStr != null) {
            global = Boolean.parseBoolean(globalStr);
        }
        String contextKeyStr = element.attributeValue("context-key");
        if (contextKeyStr == null) {
            throw new PluginParseException("context-key must be specified");
        }
        contextKey = contextKeyStr;
        String componentRefStr = element.attributeValue("component-ref");
        String classStr = element.attributeValue("class");
        if (componentRefStr != null) {
            if (classStr != null) {
                throw new PluginParseException("You may not specify both a class and a component-ref");
            }
            componentRef = componentRefStr;
        } else if (classStr == null) {
            throw new PluginParseException("You must specify a class or a component-ref");
        }
    }

    @Override
    public synchronized Object getModule() {
        // We don't cache componentRefs, because if the user wants to use a prototype component, then caching it would
        // undermine that. It's just a hash map lookup anyway.
        // We cache the ApplicationContext or the BeanAccessor (hidden in a Supplier), depending on the visibility of
        // the ApplicationContext as an OSGI service.
        if (componentRef != null) {
            if (moduleSupplier == null) {
                moduleSupplier = getModuleSupplier();
            }
            return moduleSupplier.get();
        } else {
            if (component == null) {
                component = ((ContainerManagedPlugin) getPlugin())
                        .getContainerAccessor()
                        .createBean(getModuleClass());
            }
            return component;
        }
    }

    private Supplier<Object> getModuleSupplier() {
        ApplicationContext applicationContext = getApplicationContext();
        if (applicationContext != null) {
            return () -> applicationContext.getBean(componentRef);
        } else if (getPlugin() instanceof ContainerManagedPlugin containerManagedPlugin
                && containerManagedPlugin.getContainerAccessor() != null) {
            return () -> containerManagedPlugin.getContainerAccessor().getBean(componentRef);
        } else {
            return () -> null;
        }
    }

    private ApplicationContext getApplicationContext() {
        // Get the bundles applicationContext
        OsgiPlugin osgiPlugin = (OsgiPlugin) getPlugin();
        BundleContext bundleContext = osgiPlugin.getBundle().getBundleContext();
        try {
            // Gemini (nee: Spring DM) will create an OSGi service for the application context by default
            // this is us leveraging that fact to get it
            // https://github.com/eclipse-gemini/gemini.blueprint/blob/3.0.0.M01/docs/src/docbkx/reference/bundle-app-context.xml#L136-L167
            // previously: https://docs.spring.io/spring-osgi/docs/current/reference/html/bnd-app-ctx.html
            ServiceReference[] applicationContextServiceReferences = bundleContext.getServiceReferences(
                    ApplicationContext.class.getName(),
                    "(org.springframework.context.service.name="
                            + osgiPlugin.getBundle().getSymbolicName() + ")");
            if (applicationContextServiceReferences == null || applicationContextServiceReferences.length == 0) {
                log.debug(
                        "Spring ApplicationContext for the bundle {} is not available from OSGi",
                        osgiPlugin.getBundle().getSymbolicName());
                return null;
            }
            if (applicationContextServiceReferences.length != 1) {
                log.error(
                        "Spring DM is being evil, there is not exactly one ApplicationContext for the bundle {}, there are {}",
                        osgiPlugin.getBundle().getSymbolicName(),
                        applicationContextServiceReferences.length);
            }
            return (ApplicationContext) bundleContext.getService(applicationContextServiceReferences[0]);
        } catch (InvalidSyntaxException ise) {
            log.error("Bad filter", ise);
        }
        return null;
    }

    @Override
    public synchronized void disabled() {
        super.disabled();
        component = null;
        moduleSupplier = null;
    }

    public boolean isGlobal() {
        return global;
    }

    public String getContextKey() {
        return contextKey;
    }
}
