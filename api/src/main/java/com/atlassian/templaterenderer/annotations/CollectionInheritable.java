package com.atlassian.templaterenderer.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A meta annotation that indicates to the annotation uberspect that the annotation should be retained with every item
 * in a returned collection
 *
 * @deprecated since 6.2.2, use {@code com.atlassian.velocity.htmlsafe.annotations.CollectionInheritable} instead. This
 * annotation is already NOT recognised in Confluence 9.0+ and Jira 10.0+.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.ANNOTATION_TYPE)
@Deprecated(forRemoval = true)
public @interface CollectionInheritable {}
