package com.atlassian.templaterenderer.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Declares that this method returns an object that does not require encoding if it is printed to a HTML document via
 * its {@link Object#toString()} method
 *
 * @deprecated since 1.1, use {@code com.atlassian.velocity.htmlsafe.HtmlSafe} instead. This annotation is already
 * NOT recognised in Confluence 9.0+ and Jira 10.0+.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@ReturnValueAnnotation
@CollectionInheritable
@Deprecated(forRemoval = true)
public @interface HtmlSafe {}
