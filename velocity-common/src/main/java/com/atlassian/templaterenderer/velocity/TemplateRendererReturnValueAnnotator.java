package com.atlassian.templaterenderer.velocity;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import com.atlassian.velocity.htmlsafe.annotations.ReturnValueAnnotation;
import com.atlassian.velocity.htmlsafe.introspection.MethodAnnotator;

/**
 * @deprecated since 6.2.2, use {@link com.atlassian.velocity.htmlsafe.introspection.ReturnValueAnnotator} instead.
 */
@Deprecated(forRemoval = true)
public class TemplateRendererReturnValueAnnotator implements MethodAnnotator {
    private static final Logger log = LoggerFactory.getLogger(TemplateRendererReturnValueAnnotator.class);

    private final LoadingCache<Annotation, Boolean> annotationCache = CacheBuilder.newBuilder()
            .weakKeys()
            .build(new CacheLoader<Annotation, Boolean>() {
                @Override
                public Boolean load(final Annotation annotation) {
                    return (annotation.annotationType().isAnnotationPresent(ReturnValueAnnotation.class)
                            || annotation
                                    .annotationType()
                                    .isAnnotationPresent(
                                            com.atlassian.templaterenderer.annotations.ReturnValueAnnotation.class));
                }
            });

    public Collection<Annotation> getAnnotationsForMethod(Method method) {
        Collection<Annotation> returnValueAnnotations = new HashSet<>();

        for (Annotation annotation : method.getAnnotations()) {
            if (annotationCache.getUnchecked(annotation)) {
                returnValueAnnotations.add(annotation);
            }
        }
        return Collections.unmodifiableCollection(returnValueAnnotations);
    }
}
