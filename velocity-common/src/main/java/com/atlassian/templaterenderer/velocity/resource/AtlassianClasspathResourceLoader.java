package com.atlassian.templaterenderer.velocity.resource;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.runtime.resource.loader.util.FileAllowlistHelper;
import org.apache.velocity.runtime.resource.loader.util.FileTypeAllowlistHelper;
import org.apache.velocity.runtime.util.ConfigUtil;
import org.apache.velocity.util.ClassUtils;
import org.apache.velocity.util.ExceptionUtils;
import org.osgi.framework.Bundle;

import com.atlassian.annotations.VisibleForTesting;

import static java.util.Objects.isNull;
import static org.apache.velocity.runtime.RuntimeConstants.CLASSPATH_RESOURCE_FILETYPE_ALLOWLIST;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_TRUSTED_PROTOCOLS;

import static com.atlassian.plugin.internal.util.PluginUtils.isAtlassianDevMode;

/**
 * Based off {@link ClasspathResourceLoader}, all products should use this instead.
 * <p>
 * We needed to diverge from what's in the engine to allow for Atlassian-specific concerns.
 * <p>
 * If you make changes here, please try to put them into the engine
 * @since 6.0.1
 */
public class AtlassianClasspathResourceLoader extends ClasspathResourceLoader {
    private FileAllowlistHelper fileAllowlistHelper = null;
    private FileTypeAllowlistHelper fileTypeAllowlistHelper = null;
    private List<String> trustedResourceProtocols = null;

    @Override
    public void init(ExtendedProperties configuration) {
        this.fileAllowlistHelper = new FileAllowlistHelper(rsvc);
        this.fileTypeAllowlistHelper = new FileTypeAllowlistHelper(rsvc, CLASSPATH_RESOURCE_FILETYPE_ALLOWLIST);
        this.trustedResourceProtocols = ConfigUtil.getStrings(rsvc, RESOURCE_TRUSTED_PROTOCOLS);

        if (this.log.isTraceEnabled()) {
            this.log.trace("AtlassianClasspathResourceLoader : initialization complete.");
        }
    }

    /**
     * <b>WARNING</b> This should only ever fetch files from the context classloader and the classloader of this file.
     * This is how we know that within ATR that it will only ever fetch files from the plugin using ATR. Not only is
     * this great for our sanity (predictable and isolated plugin behaviour), but is also a security requirement to
     * prevent circumventing the file and file type allowlist.
     */
    @Override
    public InputStream getResourceStream(String name) throws ResourceNotFoundException {
        if (isNull(name) || name.isBlank()) {
            throw new ResourceNotFoundException("No template name provided");
        }

        InputStream result = null;
        try {
            URL resourceUrl = ClassUtils.getResource(this.getClass(), name);
            if (!this.trustedResourceProtocols.contains(resourceUrl.getProtocol())) {
                if (!this.fileTypeAllowlistHelper.isValidFileType(name)) {
                    throw new ResourceNotFoundException(
                            "AtlassianClasspathResourceLoader : banned file type on template " + name);
                }

                if (!this.fileAllowlistHelper.isAllowed(name)) {
                    if (!isAtlassianDevMode()) {
                        throw new ResourceNotFoundException(
                                "AtlassianClasspathResourceLoader : This template is not allowed: " + name);
                    }

                    if (!templateExistsInAOsgiPlugin(
                            name, Thread.currentThread().getContextClassLoader())) {
                        log.error(
                                "Template is not on the allowlist, but it was asked to be loaded. If you just added the file, first re-install the plugin. The template was: "
                                        + name);
                        throw new ResourceNotFoundException(
                                "AtlassianClasspathResourceLoader : This template is not allowed: " + name);
                    }

                    log.debug(
                            "This template is not in the allowlist, but it exists inside of the plugin so we'll let it slide in dev mode, template is "
                                    + name);
                }
            }

            result = ClassUtils.getResourceAsStream(this.getClass(), name);
        } catch (Exception exception) {
            throw (ResourceNotFoundException) ExceptionUtils.createWithCause(
                    ResourceNotFoundException.class, "problem with template: " + name, exception);
        }

        if (isNull(result)) {
            throw new ResourceNotFoundException("AtlassianClasspathResourceLoader Error: cannot find resource " + name);
        }

        return result;
    }

    /**
     * The {@link Thread#getContextClassLoader()} could be any of:
     * <ul>
     *     <li>{@link com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor}.BundleClassLoader</li>
     *     <li>{@link com.atlassian.templaterenderer.BundleClassLoaderAccessor}.BundleClassLoader</li>
     *     <li>{@link com.atlassian.plugin.util.ChainingClassLoader}</li>
     *     <li>{@link com.atlassian.templaterenderer.velocity.CompositeClassLoader}</li>
     *     <li>{@link org.apache.felix.framework.BundleWiringImpl.BundleClassLoader}</li>
     *     <li>{@code org.eclipse.gemini.blueprint.util.BundleDelegatingClassLoader}</li>
     *     <li>{@code org.apache.catalina.loader.ParallelWebappClassLoader}</li>
     * </ul>
     *
     * Only the first two have a method of loading a file from the filesystem instead of the bundle, so they are the
     * only two which would need us to call this method. The rest would pass the trusted protocols check in
     * {@link AtlassianClasspathResourceLoader#getResourceStream} above
     */
    @VisibleForTesting
    static boolean templateExistsInAOsgiPlugin(String name, ClassLoader classLoader) {
        var isWrappedInBundleClassLoaderAccessor = classLoader
                        .getClass()
                        .getCanonicalName()
                        .startsWith(com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor.class.getCanonicalName())
                || classLoader
                        .getClass()
                        .getCanonicalName()
                        .startsWith(
                                // Hard-coded because the API is in a plugin, and we want to be able to use this in the
                                // system bundle
                                "com.atlassian.templaterenderer.BundleClassLoaderAccessor");

        if (!isWrappedInBundleClassLoaderAccessor) {
            return false;
        }

        try {
            var privateBundleField = classLoader.getClass().getDeclaredField("bundle");
            privateBundleField.setAccessible(true);
            var uncastBundleObject = privateBundleField.get(classLoader);
            var bundle = (Bundle) uncastBundleObject;

            // the system bundle is not a plugin - this does not get a carve-out in dev mode
            if (bundle.getBundleId() == 0L) {
                return false;
            }

            return !isNull(bundle.getResource(name));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}
