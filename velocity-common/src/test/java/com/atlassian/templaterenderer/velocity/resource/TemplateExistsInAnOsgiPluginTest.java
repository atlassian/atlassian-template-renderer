package com.atlassian.templaterenderer.velocity.resource;

import java.net.URL;
import java.util.UUID;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.osgi.framework.Bundle;

import com.atlassian.plugin.util.resource.NoOpAlternativeResourceLoader;

import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TemplateExistsInAnOsgiPluginTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    Bundle bundle;

    String someTemplateName;

    URL someUrl = this.getClass().getResource("");

    @Before
    public void setup() {
        someTemplateName = UUID.randomUUID().toString();
    }

    @Test
    public void testGettingTheBundle_shouldWorkWithTheAtrBundleClassLoaderAccessor() {
        // Given some bundle in com.atlassian.templaterenderer.BundleClassLoaderAccessor
        when(bundle.getBundleId()).thenReturn(1L);
        when(bundle.getResource(anyString())).thenReturn(someUrl);
        ClassLoader classLoader = com.atlassian.templaterenderer.BundleClassLoaderAccessor.getClassLoader(bundle);

        // When checking
        AtlassianClasspathResourceLoader.templateExistsInAOsgiPlugin(someTemplateName, classLoader);

        // Then we want to see it successfully get the bundle
        verify(bundle).getResource(someTemplateName);
    }

    @Test
    public void testGettingTheBundle_shouldWorkWithTheAtlassianPluginsBundleClassLoaderAccessor() {
        // Given some bundle in com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor
        when(bundle.getBundleId()).thenReturn(1L);
        when(bundle.getResource(anyString())).thenReturn(someUrl);
        ClassLoader classLoader = com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor.getClassLoader(
                bundle, new NoOpAlternativeResourceLoader());

        // When checking
        AtlassianClasspathResourceLoader.templateExistsInAOsgiPlugin(someTemplateName, classLoader);

        // Then we want to see it successfully get the bundle
        verify(bundle).getResource(someTemplateName);
    }

    @Test
    public void testOtherClassloaders_shouldNotBlowUp_soThatWeCanUseThisInTheSystemBundle() {
        ClassLoader classLoader = this.getClass().getClassLoader();

        // Should not throw an exception
        AtlassianClasspathResourceLoader.templateExistsInAOsgiPlugin(someTemplateName, classLoader);
    }

    @Test
    public void testOtherClassloaders_shouldNotBeAllowed_soThatThereIsNoSecurityHoleBypassingTheAllowlists() {
        ClassLoader classLoader = this.getClass().getClassLoader();

        boolean templateExistsInAOsgiPlugin =
                AtlassianClasspathResourceLoader.templateExistsInAOsgiPlugin(someTemplateName, classLoader);

        assertFalse(templateExistsInAOsgiPlugin);
    }
}
