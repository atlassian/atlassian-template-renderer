package com.atlassian.templaterenderer.velocity.resource;

import java.io.FileWriter;
import java.io.IOException;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.junit.Test;

import static java.lang.Boolean.TRUE;
import static java.util.Objects.requireNonNull;
import static org.apache.velocity.runtime.RuntimeConstants.CLASSPATH_RESOURCE_FILETYPE_ALLOWLIST;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILETYPE_ALLOWLIST_ENABLE;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILE_ALLOWLIST;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILE_ALLOWLIST_ENABLE;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_LOADER;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_TRUSTED_PROTOCOLS;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Effectively a clone of {@code ResourceFileAndFiletypeAllowlistTest} from our Velocity fork repo just for
 * {@link AtlassianClasspathResourceLoader} with extra test cases for Atlassian-specific concerns
 */
public class AtlassianClasspathResourceLoaderTest {
    private static final String SIMPLE_VM = "simple.vm";
    private static final String TEST_TXT = "test.txt";
    private static final String VM_GLOBAL_LIBRARY_VM = "VM_global_library.vm";

    // ------------------- Atlassian specific ----------------

    @Test
    public void testResourceLoader_shouldNotAllowBypassingOutsideOfDevMode() {
        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, TRUE);

        // then should be no problem
        assertFalse(velocityEngine.resourceExists(SIMPLE_VM));
    }

    @Test
    public void testResourceLoader_shouldAllowBypassingInDevModeIfTheFileIsInThePlugin() {

        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, TRUE);

        // then should be no problem
        assertFalse(velocityEngine.resourceExists(SIMPLE_VM));
    }

    // ------------------- Cloned tests ----------------

    @Test
    public void testResourceLoader_shouldAllowAllFilesAndFiletypesByDefault() {
        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        assertNotEquals(velocityEngine.getProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE), TRUE);
        assertNotEquals(velocityEngine.getProperty(RESOURCE_FILE_ALLOWLIST_ENABLE), TRUE);

        // then should be no problem
        assertTrue(velocityEngine.resourceExists(SIMPLE_VM));
    }

    @Test
    public void testResourceLoader_shouldBlockRandomFiletypesOOTB() {
        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        // when
        velocityEngine.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // then
        assertFalse(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    public void testResourceLoaderFileTypeAllowlist_shouldBeConfigurable() {
        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        Velocity.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(CLASSPATH_RESOURCE_FILETYPE_ALLOWLIST, ".txt");

        // then
        assertTrue(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    public void testResourceLoader_shouldBlockRandomFilesOOTB() {
        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(RESOURCE_FILE_ALLOWLIST, VM_GLOBAL_LIBRARY_VM);

        // then
        assertFalse(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    public void testResourceLoaderFileAllowlist_shouldBeConfigurable() {
        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(RESOURCE_FILE_ALLOWLIST, "VM_global_library.vm,simple.vm");

        // then
        assertFalse(velocityEngine.resourceExists(SIMPLE_VM));
    }

    @Test
    public void testResourceLoaderFileAndFiletypeAllowlist_shouldRespectTrustedProtocols() {
        // given
        VelocityEngine velocityEngine = newEngineWithAtlassianClasspathResourceLoader();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");
        velocityEngine.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // when
        velocityEngine.setProperty(RESOURCE_TRUSTED_PROTOCOLS, "file");

        // then
        assertTrue(velocityEngine.resourceExists(TEST_TXT));
    }

    private VelocityEngine newEngineWithAtlassianClasspathResourceLoader() {
        VelocityEngine velocityEngine = new VelocityEngine();

        velocityEngine.setProperty(RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty(
                "classpath.resource.loader.class", AtlassianClasspathResourceLoader.class.getCanonicalName());

        try {
            try (FileWriter writer = newClasspathFileWriter(SIMPLE_VM)) {
                writer.write("hi");
            }
            try (FileWriter writer = newClasspathFileWriter(TEST_TXT)) {
                writer.write("hi");
            }
            try (FileWriter writer = newClasspathFileWriter(VM_GLOBAL_LIBRARY_VM)) {
                writer.write("hi");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return velocityEngine;
    }

    private FileWriter newClasspathFileWriter(String filename) throws IOException {
        String basePath =
                requireNonNull(getClass().getClassLoader().getResource("")).getPath();
        String absoluteFilename = basePath + filename;
        return new FileWriter(absoluteFilename);
    }
}
