package com.atlassian.templaterenderer.velocity.integrationtest;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.hamcrest.Matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class TestRunner {
    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");

    public static String getContent(String servletName) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        String url = String.format("http://localhost:%s%s/plugins/servlet/%s", PORT, CONTEXT, servletName);
        HttpGet method = new HttpGet(url);
        HttpResponse response = client.execute(method);
        int status = response.getStatusLine().getStatusCode();

        assertEquals("Should be able to retrieve retrieve " + url, HttpStatus.SC_OK, status);

        HttpEntity entity = response.getEntity();
        String responseAsString = EntityUtils.toString(entity, "UTF-8");
        return responseAsString;
    }

    public static void runServletCheck(String servletName, String expectedString) throws IOException {
        String content = getContent(servletName);
        assertThat(content, Matchers.containsString(expectedString));
    }
}
