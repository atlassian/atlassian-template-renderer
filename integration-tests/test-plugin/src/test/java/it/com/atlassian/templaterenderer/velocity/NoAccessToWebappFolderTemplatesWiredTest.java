package it.com.atlassian.templaterenderer.velocity;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.runner.RunWith;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateRenderer;

import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.util.Objects.isNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

@RunWith(AtlassianPluginsTestRunner.class)
public class NoAccessToWebappFolderTemplatesWiredTest {

    private final TemplateRenderer templateRenderer;

    public NoAccessToWebappFolderTemplatesWiredTest(TemplateRenderer renderer) {
        this.templateRenderer = renderer;
    }

    /**
     * We should only load files that exist in the plugin bundles, otherwise that would defeat the hard work we've done
     * creating an allowlist for template files directly on the filesystem
     * <p>
     * Here we're testing the second part of the chain
     * <ol>
     *     <li>We use a specific classpath resource loader</li>
     *     <li><b>This resource loader only can only access the files in the plugin</b></li>
     * </ol>
     * The rest is tested in {@code VelocityTemplateRendererImplTest#shouldUseASetOfTrustedResourceLoaders()}
     * </p>
     * The second reason for this test is to avoid the madness that Jira and Confluence found themselves in. Using one
     * of their Velocity engines means someone can load a template just based off name, regardless of which OSGi bundle
     * it comes from. There's no guarantee which one is used, and it effectively makes every template in the system
     * grey API (so you're worried about breaking vendors and/or other plugins).
     */
    @Test
    public void testTemplateFilesInTheWebapp_shouldNotBeRenderedByAtlassianTemplateRenderer() throws IOException {
        Path newTemplateFilePath = null;
        try {
            var testPluginBundleClassLoader = this.getClass().getClassLoader();
            var webAppClassLoader = testPluginBundleClassLoader.getParent();
            var folderOnWebAppClassPath = webAppClassLoader.getResource("").getFile();
            var templateFilename = "on-webapp-classpath.vm";
            newTemplateFilePath = Paths.get(folderOnWebAppClassPath, templateFilename);

            // given
            Files.writeString(newTemplateFilePath, "hello world!", CREATE_NEW);
            assertNotNull(webAppClassLoader.getResource(templateFilename));

            var writer = new StringWriter();
            // when
            ThrowingRunnable renderTemplateFile = () -> {
                templateRenderer.render(templateFilename, writer);
            };

            // then
            assertFalse(
                    "The classloader should not even be able to see it to play it safe",
                    templateRenderer.resolve(templateFilename));
            assertThrows("The template should not be render-able at all", RenderingException.class, renderTemplateFile);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            // cleanup
            if (!isNull(newTemplateFilePath)) {
                Files.deleteIfExists(newTemplateFilePath);
            }
        }
    }
}
