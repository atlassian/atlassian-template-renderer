package it.com.atlassian.templaterenderer.velocity;

import java.util.List;
import java.util.Optional;

import org.junit.BeforeClass;
import org.junit.Test;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static java.util.Arrays.asList;
import static javax.ws.rs.core.Response.Status;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class VelocityTemplateRendererMetricTest {
    private static Optional<String> templateRendererBeans;

    @BeforeClass
    public static void setUp() {
        String operationsResourceUrl = getNoisyNeighbourUrl("/admin");
        List<String> operationTask = asList("TEMPLATE_RENDERER");

        adminJsonRequest()
                .given()
                .body(operationTask)
                .contentType(ContentType.JSON)
                .when()
                .post(operationsResourceUrl)
                .then()
                .log()
                .ifValidationFails()
                .statusCode(Status.NO_CONTENT.getStatusCode());

        List<String> mBeanNames = asList(adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .when()
                .get(getNoisyNeighbourUrl("/jmx"))
                .as(String[].class));

        templateRendererBeans = mBeanNames.stream()
                // Match on the template renderer task metric
                .filter(n -> n.contains("name=webTemplateRenderer"))
                // Match it's from the Atlassian Diagnostics' plugin
                .filter(n -> n.contains("com.atlassian.diagnostics.noisy-neighbour-plugin"))
                // Contains the template renderer name
                .filter(n -> n.contains("velocity"))
                // Contains the renderer template name
                .filter(n -> n.contains("templates/template-renderer-view.vm"))
                .findFirst();
    }

    @Test
    public void whenTemplateRenderIsCalled_beanContainsTemplateName() {
        assertThat("Template name exists", templateRendererBeans.isPresent(), is(true));
    }

    @Test
    public void whenTemplateRenderIsCalled_beanCountIncreasesByOne() {
        Response templateRendererAttributes = adminJsonRequest()
                .queryParam("name", templateRendererBeans.get())
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .when()
                .get(getNoisyNeighbourUrl("/jmx/search"));

        assertThat(
                "Template renderer is counted",
                Double.parseDouble(templateRendererAttributes.jsonPath().get("Count")),
                is(1d));
    }

    private static String getNoisyNeighbourUrl(String path) {
        String baseurl = System.getProperty("baseurl", "http://localhost:5990/refapp");

        return baseurl + "/rest/noisyneighbour/latest" + path;
    }

    private static String getAdminUser() {
        return "admin";
    }

    private static String getAdminPassword() {
        return "admin";
    }

    private static RequestSpecification adminJsonRequest() {
        return RestAssured.given()
                .auth()
                .preemptive()
                .basic(getAdminUser(), getAdminPassword())
                .contentType(ContentType.JSON);
    }
}
