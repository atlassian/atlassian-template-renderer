package it.com.atlassian.templaterenderer.velocity;

import java.lang.reflect.Proxy;

import org.apache.velocity.util.introspection.MethodTranslator;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.velocity.allowlist.uberspect.AtlassianMethodTranslator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AtlassianPluginsTestRunner.class)
public class VelocityAllowlistMethodTranslatorTest {

    private final UserManager userManager;
    private final MethodTranslator methodTranslator;

    public VelocityAllowlistMethodTranslatorTest(UserManager userManager) throws Exception {
        this.userManager = userManager;
        this.methodTranslator = new AtlassianMethodTranslator();
    }

    @Test
    public void translatesProxyMethod() throws Exception {
        var expectedDeclaringClass = "com.atlassian.refapp.sal.user.RefImplUserManager";
        var expectedMethodName = "getRemoteUserKey";

        assertTrue(Proxy.isProxyClass(userManager.getClass()));
        var proxyMethod = userManager.getClass().getMethod(expectedMethodName);
        assertNotEquals(expectedDeclaringClass, proxyMethod.getDeclaringClass().getName());

        var translatedMethod = methodTranslator.getTranslatedMethod(userManager, proxyMethod);
        assertEquals(
                expectedDeclaringClass, translatedMethod.getDeclaringClass().getName());
        assertEquals(expectedMethodName, translatedMethod.getName());
    }
}
