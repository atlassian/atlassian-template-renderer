package it.com.atlassian.templaterenderer.velocity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.templaterenderer.velocity.integrationtest.TestRunner;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class TemplateFileChangesDevelopmentModeTest {
    Path templateFilePath;
    byte[] originalTemplateFileContent;

    @Before
    public void setup() throws IOException {
        templateFilePath = Paths.get("src", "main", "resources", "velocity-1.6.vm");
        originalTemplateFileContent = Files.readAllBytes(templateFilePath);
    }

    /**
     * We like keeping engineers happy
     */
    @Test
    public void testMakingChangesToTheTemplateFiles_shouldBeReflectedWithoutReinstallingThePlugin() throws IOException {
        // given a change to a template file
        var expectedContent = "development mode works! " + UUID.randomUUID();
        Files.write(templateFilePath, expectedContent.getBytes(UTF_8), TRUNCATE_EXISTING);

        // when they go to test their changes
        var servedContent = TestRunner.getContent("velocity1.6");

        // they can see them without reinstalling the plugin
        assertThat(servedContent, containsString(expectedContent));
    }

    @After
    public void teardown() throws IOException {
        Files.write(templateFilePath, originalTemplateFileContent, TRUNCATE_EXISTING);
    }
}
