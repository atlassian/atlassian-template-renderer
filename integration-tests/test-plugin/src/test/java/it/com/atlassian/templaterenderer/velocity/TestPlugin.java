package it.com.atlassian.templaterenderer.velocity;

import java.io.IOException;
import java.util.regex.Pattern;

import org.junit.Test;

import com.atlassian.templaterenderer.velocity.integrationtest.TestRunner;

import static org.junit.Assert.assertTrue;

public class TestPlugin {
    @Test
    public void run() throws IOException {
        TestRunner.runServletCheck("velocity1.6", "velocity-1.6");
    }

    /**
     * Confirm that Velocity's <code>directive.set.null.allowed</code> is not enabled.
     */
    @Test
    public void confirmThatSetNullAllowedIsFalse() throws IOException {
        String content = TestRunner.getContent("velocity1.6");

        Pattern p = Pattern.compile("<li id=\"set-null-enabled\">\\s*not null\\s*</li>");

        assertTrue(p.matcher(content).find());
    }

    /**
     * Confirm that &lt;script&gt; tags are rendered correctly (not escaped).
     * It's not necessary to use users/admin, any page would do.
     */
    @Test
    public void checkThatScriptTagsAreNotEscaped() throws IOException {
        TestRunner.runServletCheck("users/admin", "<script>");
    }

    @Test
    public void runPanelTest() throws IOException {
        TestRunner.runServletCheck("users/admin", "<li id=\"engine\">velocity-1.6</li>");
    }
}
