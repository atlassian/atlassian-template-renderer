package it.com.atlassian.templaterenderer.velocity;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.templaterenderer.velocity.allowlist.ClassTestObject;
import com.atlassian.templaterenderer.velocity.allowlist.ExtendClassTestObject;
import com.atlassian.templaterenderer.velocity.allowlist.ExtendMethodTestObject;
import com.atlassian.templaterenderer.velocity.allowlist.MethodTestObject;
import com.atlassian.templaterenderer.velocity.allowlist.pkg.AltPackageTestObject;
import com.atlassian.templaterenderer.velocity.allowlist.pkg.PackageTestObject;
import com.atlassian.velocity.allowlist.api.internal.PluginAllowlist;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(AtlassianPluginsTestRunner.class)
public class VelocityAllowlistWiredTest {

    private final PluginAllowlist pluginAllowlist;

    public VelocityAllowlistWiredTest(PluginAllowlist pluginAllowlist) {
        this.pluginAllowlist = pluginAllowlist;
    }

    private static List<Method> getDeclMethods(Class<?> clazz) {
        return Arrays.asList(clazz.getDeclaredMethods());
    }

    @Test
    public void classAllowlist() throws Exception {
        assertTrue(pluginAllowlist.isAllowlistedClassPackage(ClassTestObject.class));

        // If class is allowlisted, all methods declared in that class should be allowlisted
        getDeclMethods(ClassTestObject.class).forEach(method -> {
            assertTrue(pluginAllowlist.isAllowlisted(method));

            // Method-specific allowlist will still return false
            assertFalse(pluginAllowlist.isAllowlistedMethod(method));
        });
    }

    @Test
    public void classAllowlist_inheritance() throws Exception {
        assertFalse(pluginAllowlist.isAllowlistedClassPackage(ExtendClassTestObject.class));
        assertTrue(pluginAllowlist.isAllowlistedClassPackage(ClassTestObject.class));

        // Superclass is allowlisted, but method3 is overridden so should not be allowlisted
        var method1 = ExtendClassTestObject.class.getMethod("testMethod", String.class, long.class);
        var method2 = ExtendClassTestObject.class.getMethod("testMethod2", Integer.class);
        var method3 = ExtendClassTestObject.class.getMethod("testMethod3");

        assertFalse(pluginAllowlist.isAllowlisted(method3));

        List.of(method1, method2).forEach(method -> assertTrue(pluginAllowlist.isAllowlisted(method)));
    }

    @Test
    public void methodAllowlist() throws Exception {
        // Only method1 and method3 are allowlisted in plugin descriptor
        var method1 = MethodTestObject.class.getMethod("testMethod", String.class, long.class);
        var method2 = MethodTestObject.class.getMethod("testMethod2", Integer.class);
        var method3 = MethodTestObject.class.getMethod("testMethod3");

        List.of(method1, method3).forEach(method -> {
            assertTrue(pluginAllowlist.isAllowlisted(method));
            assertTrue(pluginAllowlist.isAllowlistedMethod(method));
        });

        assertFalse(pluginAllowlist.isAllowlisted(method2));
        assertFalse(pluginAllowlist.isAllowlistedMethod(method2));

        assertFalse(pluginAllowlist.isAllowlistedClassPackage(MethodTestObject.class));
    }

    @Test
    public void methodAllowlist_inheritance() throws Exception {
        // Method1 and method3 are allowlisted in superclass, but method3 is overridden, so only method1 should be
        // allowlisted
        var method1 = ExtendMethodTestObject.class.getMethod("testMethod", String.class, long.class);
        var method2 = ExtendMethodTestObject.class.getMethod("testMethod2", Integer.class);
        var method3 = ExtendMethodTestObject.class.getMethod("testMethod3");

        assertTrue(pluginAllowlist.isAllowlisted(method1));
        assertTrue(pluginAllowlist.isAllowlistedMethod(method1));

        List.of(method2, method3).forEach(method -> {
            assertFalse(pluginAllowlist.isAllowlisted(method));
            assertFalse(pluginAllowlist.isAllowlistedMethod(method));
        });

        assertFalse(pluginAllowlist.isAllowlistedClassPackage(MethodTestObject.class));
    }

    @Test
    public void packageAllowlist() throws Exception {
        // If package is allowlisted, all methods declared in all classes should be allowlisted
        List.of(PackageTestObject.class, AltPackageTestObject.class).forEach(clazz -> {
            assertTrue(pluginAllowlist.isAllowlistedClassPackage(clazz));

            getDeclMethods(clazz).forEach(method -> {
                assertTrue(pluginAllowlist.isAllowlisted(method));

                // Method-specific allowlist will still return false
                assertFalse(pluginAllowlist.isAllowlistedMethod(method));
            });
        });
    }
}
