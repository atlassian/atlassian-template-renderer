package it.com.atlassian.templaterenderer.velocity;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.templaterenderer.velocity.integrationtest.TestRunner;

import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

import static com.atlassian.templaterenderer.velocity.VelocityTestServlet.TEMPLATE_FILENAME;

public class NewTemplateFileDevelopmentModeTest {
    private static final String NOT_BUNDLED_IN_JAR_VM = "not-bundled-in-jar.vm";
    Path templateFilePath;

    @Before
    public void setup() {
        templateFilePath = Paths.get("src", "main", "resources", NOT_BUNDLED_IN_JAR_VM);
    }

    @Test
    public void testAddingTemplateFiles_shouldBeAccessibleInstantly() throws IOException {
        var templateContent = "Should be picked up by the Velocity engine";
        Files.writeString(templateFilePath, templateContent, CREATE_NEW);

        // when they go to test their changes
        var servedContent = TestRunner.getContent("velocity1.6?" + TEMPLATE_FILENAME + "=" + NOT_BUNDLED_IN_JAR_VM);

        // they can see them without reinstalling the plugin
        assertThat(servedContent, containsString(templateContent));
    }

    @After
    public void teardown() throws IOException {
        Files.deleteIfExists(templateFilePath);
    }
}
