package com.atlassian.templaterenderer.velocity;

import java.io.IOException;
import java.util.Optional;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.templaterenderer.TemplateRenderer;

public class VelocityTestServlet extends HttpServlet {
    @VisibleForTesting
    public static final String TEMPLATE_FILENAME = "templateFilename";

    private final TemplateRenderer templateRenderer;

    public VelocityTestServlet(TemplateRenderer renderer) {
        this.templateRenderer = renderer;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        var templateFilename =
                Optional.ofNullable(request.getParameter(TEMPLATE_FILENAME)).orElse("velocity-1.6.vm");
        templateRenderer.render(templateFilename, response.getWriter());
    }
}
