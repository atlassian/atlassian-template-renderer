package com.atlassian.templaterenderer.velocity.allowlist;

public class ExtendMethodTestObject extends MethodTestObject {

    @Override
    public String testMethod3() {
        return "testMethod3-overridden";
    }
}
