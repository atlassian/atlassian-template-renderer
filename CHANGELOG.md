# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [6.4.0]

### Changed

* [ECO-422](https://bulldog.internal.atlassian.com/browse/ECO-422) use ContainerManagedPlugin's BeanAccessor to get beans if the OSGI Bundle ApplicationContext is not exposed.

## [6.3.1]

### Changed

* Additionally recognise `atlassian.plugin.velocity.method.allowlist.extra` system property

## [6.3.0]

### Added

* [CONFSERVER-97796](https://jira.atlassian.com/browse/CONFSERVER-97796) Introduce
  `atlassian.velocity.method.allowlist.extra` system property to allow for additional method allowlist entries to be
  specified. This mechanism is useful for Confluence customers/administrators writing Velocity based User Macros.

## [6.2.1]

### Changed
* Removed inconsequential forceful override of parser.pool.class

## [6.2.0]

### Added

* [DCA11Y-1177](https://hello.jira.atlassian.cloud/browse/DCA11Y-1177) HTML traceability comments

## [6.1.1]
* Removed extra import clauses from plugin description; to avoid extra dependencies to load

## [6.1.0]
### Changed
* Updated `PluginAwareSecureIntrospector` to ensure plugin allowlist entries are not internally cached by the Velocity
  engine (Requires Velocity 1.6.4-atlassian-34)
* `velocity-allowlist` modules are more strictly validated and completely disabled if any invalid entries are found
* Deprecated `PluginAllowlistRegistrar` and optimised performance of `VelocityAllowlistModuleDescriptor` and
  `PluginAllowlistConfigurator`

### Added
* Introduce the `atlassian.velocity.method.allowlist.debug` system property which when set to `true` will disable the
  method allowlist but continue to log errors as if it were enabled. This is achieved by enabling the corresponding
  allowlist debug option in the engine. This option exists to enable convenient collection of invoked methods to inform
  allowlist configuration. The added system property is particularly useful for vendors whom cannot easily change the
  Velocity configuration directly.

## [6.0.4]
- Removed extra import clauses from plugin description; to avoid extra dependencies to load

## [6.0.3]
### BREAKING
- Fixed `AtlassianMethodTranslator` not unproxying Spring beans that were created using the `platform-spring-bundle`
  instead of the system bundle
- Fixed `AtlassianMethodTranslator` to resolve plugin imported OSGi service proxies
- Fixed `AtlassianMethodTranslator` to resolve Velocity HtmlSafe annotation preserving proxies
- Fixed `AtlassianMethodTranslator` to resolve the concrete method when the Velocity engine mistakenly resolves an
  interface or abstract method
- Note that the above changes may require allowlist entries to be updated to reflect the concrete method rather than the
  interface method; this is necessary to ensure consistency and elevated security

## [6.0.2]
### Added
- Added `HtmlSafePluginAwareSecureUberspector` helper class for products also utilising `velocity-htmlsafe`

## [6.0.1]
### Fixed
- [DCA11Y-963](https://hello.atlassian.net/browse/DCA11Y-963) Fix files loaded in dev mode being blocked by the file allowlist

## [6.0.0] - 2024-04-24
### Changed
* Bump of `platform-poms`, Atlassian Plugins and Atlassian REST to the Platform 6 milestone
* Updated the platform version to 6.0.2 (BSP-3816)
* ATR-104 API and Velocity 1.6 plugins are transformer-less
* ATR Uberspect is no longer forced to TemplateRendererHtmlAnnotationEscaper, it will respect
  the `runtime.introspector.uberspect` property unless it is the Velocity default. This is a potential breaking change!
  Please ensure you have not customised this property if you wish for ATR to use the same Uberspect as before.
* Introduce the velocity-allowlist module containing classes necessary for backing the Velocity engine Java method
  allowlist with plugin module descriptors in Atlassian DC products

## [5.0.1] - 2022-02-22
### Changed
* [CONFSERVER-65064](https://jira.atlassian.com/browse/CONFSERVER-65064) Fix for Custom Application Link Configuration not working due to XSRF Security Token Missing message
* Updated the platform version to 6.0.2 (BSP-3816)

## [5.0.0] - 2022-02-11
### Changed
* Upgrades of `platform-poms`, Atlassian Plugins 7, Webfragment 6, Webresource 6, Atlassian Annotations 4 and Atlassian Event 5 for Platform 6
* Upgrade dom4j to 2.x for Platform 6

## [4.3.0] Unreleased
* [CONFSERVER-65064](https://jira.atlassian.com/browse/CONFSERVER-65064) Fix for Custom Application Link Configuration not working due to XSRF Security Token Missing message

## [4.2.1] - 2022-02-22
### Changed
* Bump of `platform-poms`, Atlassian Plugins and Atlassian REST to the Platform 6 milestone
* Updated the platform version to 6.0.2 (BSP-3816)

## [4.2.0] - 2021-10-26
### Upmerged
* web panel render time analytic event to be async preferred

### Security
* Bump of org.apache.httpcomponents:httpclient to 4.5.13.
* Bump of junit:junit to 4.13.2
* Exclusion of commons-codec:commons-codec from dependencies due to security issue.

### Changed
* Added metric around the Velocity Template Renderer 

## [4.1.6] - 2022-01-17
### Changed
* [CONFSERVER-65064](https://jira.atlassian.com/browse/CONFSERVER-65064) Fix for Custom Application Link Configuration not working due to XSRF Security Token Missing message

## [4.1.5] - 2021-10-21
### Changed
* [SPFE-855](https://ecosystem.atlassian.net/browse/SPFE-855) Remove web panel render time events

## [4.1.4]
### Changed
* web panel render time analytic event to be async preferred

## [4.1.3]
### Changed
* Bump Platform POMs to 5.0.30
* Replace import of `com.atlassian.platform:platform` POM file with explicit dependencies 

## [4.1.2]
### Security
* Bump of org.apache.httpcomponents:httpclient to 4.5.13.
* Bump of junit:junit to 4.13.2
* Exclusion of commons-codec:commons-codec from dependencies due to security issue.

## [4.1.1]
### Changed
* SECURITY: apache-velocity-1.6.4-atlassian fork bumped with commons collection 3.2.2 bumped

## [4.1.0]
### Changed
* velocity1.6 template renderer now fires an analytic event of how long each webpanel took to render

## [4.0.4] - 2022-01-17
### Changed
* [CONFSERVER-65064](https://jira.atlassian.com/browse/CONFSERVER-65064) Fix for Custom Application Link Configuration not working due to XSRF Security Token Missing message
* Updated the platform version to 6.0.2 (BSP-3816)

## [4.0.3]
### Security
* Bump of org.apache.httpcomponents:httpclient to 4.5.13.
* Bump of junit:junit to 4.13.2
* Exclusion of commons-codec:commons-codec from dependencies due to security issue.

## [4.0.1]
### Changed
* ATR now respects product's velocity.properties and takes care of velocity uberspect restriction.
* velocity-htmlsafe bumped to 3.1.1
* velocity-htmlsafe bumped to 3.1.0 to bring SecureUberspector changes
* velocity bumped to 1.6.4-atlassian-19 to bring performance enhancement in SecureUberspector
* Moved to latest available platform 5.0.11

## [4.0.0]

### Changed
* Adds support for Java 11
* Now depends on Platform 5.0 being provided by the host product
* Removes dependence on atlassian-util-concurrent
