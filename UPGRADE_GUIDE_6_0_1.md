# 6.0.1 Upgrade guide

## For everyone

If you instantiate Velocity engines (yes, that's you especially product engineer). Use `AtlassianClasspathResourceLoader` instead of `ClasspathResourceLoader` by setting the velocity engine property `classpath.resource.loader.class` to `com.atlassian.templaterenderer.velocity.resource.AtlassianClasspathResourceLoader` for all engines.

## For host products

1. Upgrade to the v31 fork of Velocity 1.6.4 or higher.
2. Add a test that arbitrary velocity files from the webapp folder cannot be rendered unless they are allowlisted.
