# 6.1.0 Upgrade guide

## For host products (e.g. Confluence)

Update the `PluginAllowlistConfigurator` bean definition to use the new constructor which takes a `PluginAccessor`
parameter.

```java
@Resource
PluginAccessor pluginAccessor;

@Bean
PluginAllowlistConfigurator pluginAllowlistConfigurator() {
    return new PluginAllowlistConfigurator(pluginAccessor);
}
```

Note that the deprecated no-args constructor is retained and should continue to work as expected, however it is strongly
recommended to migrate to the new constructor to avoid unanticipated issues.
