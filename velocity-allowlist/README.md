# Velocity Allowlist

Velocity `1.6.4-atlassian-34` and above implements a method allowlisting capability. This module contains classes to
ease integration into Atlassian server products whilst also providing the capability to extend the Velocity engine
allowlist with methods and/or classes specified in plugin module descriptors.

## Integration steps

1. Register the `VelocityAllowlistModuleDescriptor` in your product. The exact steps may vary between products but this
   usually involves adding an entry to the following bean call -
   `com.atlassian.plugin.DefaultModuleDescriptorFactory#setModuleDescriptors`.
2. Register the `PluginAllowlistConfigurator` as a bean as this is a required dependency for the
   `VelocityAllowlistModuleDescriptor`.
3. Configure all Velocity engines to use the `PluginAwareSecureIntrospector` introspector. This can be done by
   configuring the `runtime.introspector.uberspect` property as `PluginAwareSecureUberspector` in
   `velocity-default.properties`.
   1. If your product uses ATR or otherwise requires `@HtmlSafe` recognition, you must use
      `HtmlSafePluginAwareSecureUberspector` instead (requires `velocity-htmlsafe` 5.0.0+), and additionally set
      `runtime.introspector.uberspect` in `velocity.properties` as ATR will otherwise override the value read from
      `velocity-default.properties`. All other Introspector configuration should still go in
      `velocity-default.properties`.
4. Configure `introspector.method.translator` as `AtlassianMethodTranslator` if your product utilises Spring and/or
   Hibernate as this will enable unproxying of such instances and thus more secure and consistent allowlisting.
5. Ensure your product instantiates a Velocity engine on startup to initialise the Uberspect and Introspector
   immediately. You may choose to define a bean for this purpose.
6. Register a `PluginAwareSecureIntrospector` bean, where the instance is retrieved from the Uberspect that was defined
   previously. This bean should only be registered after at least one Velocity engine has been initialised. This bean
   should have the `PluginAllowlistConfigurator` injected, and additionally registered as an event listener. Here is an
   example using Spring Java configuration:
   ```java
   @Configuration
   public class VelocityBeans {
   
       @Resource
       PluginEventManager pluginEventManager;
   
       @Resource
       PluginAccessor pluginAccessor;
   
       @Bean
       VelocityEngine velocityEngine() throws Exception {
           var velocity = new VelocityEngine();
           velocity.init();
           return velocity;
       }
   
       @Bean
       PluginAllowlistConfigurator pluginAllowlistConfigurator() {
           return new PluginAllowlistConfigurator(pluginAccessor);
       }
   
       @DependsOn("velocityEngine")
       @Bean
       PluginAwareSecureIntrospector velocityIntrospector() {
           var introspector = (PluginAwareSecureIntrospector) requireNonNull((new PluginAwareSecureUberspector()).getIntrospector());
           introspector.setPluginAllowlist(pluginAllowlistConfigurator());
           pluginEventManager.register(introspector);
           return introspector;
       }
   }
   ```
7. Expose a `GlobalMethodAllowlist` OSGi service which allows plugins to consult the allowlist in a generic way, for use
   cases similar but distinct from Velocity rendering. Here is an example using Spring Java configuration:
   ```java
   @AvailableToPlugins
   @Bean
   GlobalMethodAllowlist globalMethodAllowlist() {
      return new VelocityGlobalMethodAllowlist(velocityIntrospector());
   }
   ```

## Customisation

### Uberspect
You may integrate a custom Uberspect which extends the `SecureUberspector` into `PluginAwareSecureUberspector` by
defining a class as follows:
```java
public class CustomPluginAwareSecureUberspector extends PluginAwareSecureUberspector {
    @Override
    protected Uberspect newUberspector() {
        return new CustomSecureUberspector() {
            @Override
            public void init() {
                super.init();
                introspector = getIntrospectorInternal(log, runtimeServices);
            }
        };
    }
}
```

### Introspector
You may also use a custom Introspector which extends the `PluginAwareSecureIntrospector` by defining a class as follows:
```java
public class CustomPluginAwareSecureUberspector extends PluginAwareSecureUberspector {
    @Override
    protected Introspector newIntrospector(Log log, RuntimeServices runtimeServices) {
        return new CustomSecureIntrospector(log, runtimeServices);
    }
}
```

### Uberspect & Introspector
The above class definitions can be combined as follows:
```java
public class CustomPluginAwareSecureUberspector extends PluginAwareSecureUberspector {
    @Override
    protected Uberspect newUberspector() {
        return new CustomSecureUberspector() {
            @Override
            public void init() {
                super.init();
                introspector = getIntrospectorInternal(log, runtimeServices);
            }
        };
    }

    @Override
    protected Introspector newIntrospector(Log log, RuntimeServices runtimeServices) {
        return new CustomSecureIntrospector(log, runtimeServices);
    }
}
```
