package com.atlassian.velocity.allowlist.api.internal;

import java.lang.reflect.Method;

/**
 * Determines whether a plugin method or class is allowed to be invoked from a Velocity template, as defined by plugin
 * module descriptors.
 *
 * @since 6.0.0
 */
public interface PluginAllowlist {

    /**
     * Checks if a method is allowlisted by either the method explicitly or by its class/package.
     *
     * @since 6.1.0
     */
    default boolean isAllowlisted(Method method) {
        return isAllowlistedMethod(method) || isAllowlistedClassPackage(method.getDeclaringClass());
    }

    /**
     * Only returns {@code true} if the actual class is allowlisted, irrespective of whether all methods declared in the
     * class are individually allowlisted.
     */
    boolean isAllowlistedClassPackage(Class<?> clazz);

    /**
     * Only returns {@code true} if the specific method is allowlisted, irrespective of whether its declaring class is
     * allowlisted. Call {@link #isAllowlisted} to consider both method and class/package allowlist configuration.
     */
    boolean isAllowlistedMethod(Method method);

    /**
     * Reloads the allowlist from the plugin module descriptors.
     *
     * @since 6.1.0
     */
    void reload();
}
