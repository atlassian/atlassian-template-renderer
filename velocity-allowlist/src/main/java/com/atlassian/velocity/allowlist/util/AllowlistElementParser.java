package com.atlassian.velocity.allowlist.util;

import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.Element;

/**
 * @since 6.0.0
 * @deprecated since 6.1.0, use {@link AllowlistElementParser} instead.
 */
@Deprecated(forRemoval = true)
public class AllowlistElementParser {

    private final AllowlistElements allowlistElements;

    /**
     * @throws PluginParseException if unexpected element values encountered
     */
    public AllowlistElementParser(Element rootElement) throws IllegalArgumentException {
        this.allowlistElements = AllowlistElements.parse(rootElement);
    }

    public Set<String> getClasses() {
        return allowlistElements.getClasses();
    }

    public Set<String> getPackages() {
        return allowlistElements.getPackages();
    }

    public Map<String, Set<String>> getMethods() {
        return allowlistElements.getMethods();
    }
}
