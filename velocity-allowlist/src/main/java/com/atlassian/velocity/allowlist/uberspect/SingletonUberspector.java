package com.atlassian.velocity.allowlist.uberspect;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.velocity.runtime.RuntimeLogger;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.RuntimeServicesAware;
import org.apache.velocity.util.introspection.Info;
import org.apache.velocity.util.introspection.Introspector;
import org.apache.velocity.util.introspection.SecureIntrospectorImpl;
import org.apache.velocity.util.introspection.SecureUberspector;
import org.apache.velocity.util.introspection.Uberspect;
import org.apache.velocity.util.introspection.UberspectLoggable;
import org.apache.velocity.util.introspection.VelMethod;
import org.apache.velocity.util.introspection.VelPropertyGet;
import org.apache.velocity.util.introspection.VelPropertySet;

/**
 * Useful for enforcing a single {@link Uberspect} instance is used for all constructed Velocity engines rather than
 * creating a new instance for each engine. This is particularly useful when the {@link Uberspect} implementation is
 * expensive to create or has state that should be shared across all engines.
 * <p>
 * Simply extend this class and override {@link #newUberspector()} to return the desired {@link Uberspect}
 * implementation, then wire in the new class name using the {@code runtime.introspector.uberspect} property in the
 * Velocity configuration. If you instantiate this class directly, it will use the default {@link SecureUberspector}.
 * <p>
 * This class can also optionally track the coupled {@link Introspector} if one exists by overriding
 * {@link #newIntrospector} and replacing the Introspector construction within your Uberspector with
 * {@link #getIntrospectorInternal}. You may then call {@link #getIntrospector()} at any time to access the coupled
 * Introspector instance.
 * <p>
 * There is however one caveat, and that is that the {@link Uberspect} instance will be initialised with the
 * {@link RuntimeServices} provided by the first engine that calls {@link #init()}. It is thus important that the first
 * engine initialisation is deterministic for consistent, reproducible configuration.
 *
 * @since 6.0.0
 */
public class SingletonUberspector implements Uberspect, UberspectLoggable, RuntimeServicesAware {

    private static final Map<Class<?>, Uberspect> UBERSPECT_INSTANCES = new ConcurrentHashMap<>();
    private static final Map<Class<?>, Introspector> INTROSPECTOR_INSTANCES = new ConcurrentHashMap<>();
    private static final Map<Class<?>, Boolean> IS_INIT_MAP = new ConcurrentHashMap<>();

    private Uberspect getDelegate() {
        return UBERSPECT_INSTANCES.computeIfAbsent(this.getClass(), k -> newUberspector());
    }

    protected Uberspect newUberspector() {
        return new SecureUberspector() {
            @Override
            public void init() throws Exception {
                super.init();
                introspector = getIntrospectorInternal(log, runtimeServices);
            }
        };
    }

    protected Introspector getIntrospectorInternal(Log log, RuntimeServices runtimeServices) {
        return INTROSPECTOR_INSTANCES.computeIfAbsent(this.getClass(), k -> newIntrospector(log, runtimeServices));
    }

    protected Introspector newIntrospector(Log log, RuntimeServices runtimeServices) {
        return new SecureIntrospectorImpl(log, runtimeServices);
    }

    private boolean isInit() {
        return IS_INIT_MAP.getOrDefault(this.getClass(), false);
    }

    private void setInit() {
        IS_INIT_MAP.put(this.getClass(), true);
    }

    public Introspector getIntrospector() {
        return INTROSPECTOR_INSTANCES.get(this.getClass());
    }

    /**
     * This guarantees the Uberspector will only be initialised once but doesn't guarantee which {@link RuntimeServices}
     * it will be initialised with.
     */
    @Override
    public void init() throws Exception {
        if (!isInit()) {
            //noinspection SynchronizationOnGetClass : Intentional, extending classes should synchronize independently
            synchronized (this.getClass()) {
                if (!isInit()) {
                    getDelegate().init();
                    setInit();
                }
            }
        }
    }

    @Override
    public Iterator getIterator(Object obj, Info info) throws Exception {
        return getDelegate().getIterator(obj, info);
    }

    @Override
    public VelMethod getMethod(Object obj, String method, Object[] args, Info info) throws Exception {
        return getDelegate().getMethod(obj, method, args, info);
    }

    @Override
    public VelPropertyGet getPropertyGet(Object obj, String identifier, Info info) throws Exception {
        return getDelegate().getPropertyGet(obj, identifier, info);
    }

    @Override
    public VelPropertySet getPropertySet(Object obj, String identifier, Object arg, Info info) throws Exception {
        return getDelegate().getPropertySet(obj, identifier, arg, info);
    }

    @Override
    public void setRuntimeServices(RuntimeServices rs) {
        if (!isInit() && getDelegate() instanceof RuntimeServicesAware) {
            ((RuntimeServicesAware) getDelegate()).setRuntimeServices(rs);
        }
    }

    @Override
    public void setLog(Log log) {
        if (!isInit() && getDelegate() instanceof UberspectLoggable) {
            ((UberspectLoggable) getDelegate()).setLog(log);
        }
    }

    @Override
    public void setRuntimeLogger(RuntimeLogger logger) {
        // deprecated, no-op
    }
}
