package com.atlassian.velocity.allowlist.uberspect;

import org.apache.velocity.util.introspection.Uberspect;

import com.atlassian.velocity.htmlsafe.introspection.HtmlSafeAnnotationBoxingUberspect;

/**
 * A {@link PluginAwareSecureUberspector} subclass which also respects the functionality of
 * {@link HtmlSafeAnnotationBoxingUberspect}.
 *
 * @since 6.0.2
 */
public class HtmlSafePluginAwareSecureUberspector extends PluginAwareSecureUberspector {

    @Override
    protected Uberspect newUberspector() {
        return new HtmlSafeAnnotationBoxingUberspect() {
            @Override
            public void init() {
                super.init();
                introspector = getIntrospectorInternal(log, runtimeServices);
            }
        };
    }
}
