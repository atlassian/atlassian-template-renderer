package com.atlassian.velocity.allowlist.uberspect;

import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.introspection.Introspector;

/**
 * A {@link SingletonUberspector} subclass which uses the default
 * {@link org.apache.velocity.util.introspection.SecureUberspector} as the Uberspect delegate but with a
 * {@link PluginAwareSecureIntrospector} as the coupled Introspector.
 *
 * @since 6.0.0
 */
public class PluginAwareSecureUberspector extends SingletonUberspector {
    @Override
    protected Introspector newIntrospector(Log log, RuntimeServices runtimeServices) {
        return new PluginAwareSecureIntrospector(log, runtimeServices);
    }
}
