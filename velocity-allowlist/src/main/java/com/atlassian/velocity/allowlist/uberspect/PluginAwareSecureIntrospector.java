package com.atlassian.velocity.allowlist.uberspect;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.introspection.SecureIntrospectorImpl;
import io.atlassian.util.concurrent.Lazy;

import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.velocity.allowlist.api.internal.PluginAllowlist;

import static java.util.stream.Collectors.joining;

/**
 * Extension of {@link SecureIntrospectorImpl} which can additionally consult the {@link PluginAllowlist}. In addition
 * to configuring this Introspector for use in the Velocity engine, the engine internal instance must then be retrieved
 * and initialised by calling {@link #setPluginAllowlist} and registered as an event listener.
 *
 * @since 6.0.0
 */
public class PluginAwareSecureIntrospector extends SecureIntrospectorImpl {

    public static final String ALLOWLIST_DEBUG_PROPERTY = "atlassian.velocity.method.allowlist.debug";
    public static final String ALLOWLIST_DEBUG_PROPERTY_ALT = "atlassian.plugins.velocity.method.allowlist.debug";
    public static final String ALLOWLIST_EXTRA_PROPERTY = "atlassian.velocity.method.allowlist.extra";
    public static final String ALLOWLIST_EXTRA_PROPERTY_ALT = "atlassian.plugin.velocity.method.allowlist.extra";

    private final boolean allowlistDebugMode =
            Boolean.getBoolean(ALLOWLIST_DEBUG_PROPERTY) || Boolean.getBoolean(ALLOWLIST_DEBUG_PROPERTY_ALT);
    private final Supplier<Map<Class<?>, Set<String>>> sysPropAllowlistedMethodsRef;

    private PluginAllowlist pluginAllowlist;
    private volatile ClassLoader pluginClassLoader;
    private final AtomicBoolean isPluginFrameworkStarted = new AtomicBoolean();

    public PluginAwareSecureIntrospector(Log log, RuntimeServices runtimeServices) {
        super(log, runtimeServices);
        this.sysPropAllowlistedMethodsRef = initSysPropAllowlistedMethods();
    }

    private Supplier<Map<Class<?>, Set<String>>> initSysPropAllowlistedMethods() {
        var allowlistExtra = System.getProperty(ALLOWLIST_EXTRA_PROPERTY, "");
        var allowlistExtraAlt = System.getProperty(ALLOWLIST_EXTRA_PROPERTY_ALT, "");
        var allowlistExtraFinal = allowlistExtra + (!allowlistExtraAlt.isBlank() ? "," : "") + allowlistExtraAlt;
        String[] methodArray = !allowlistExtraFinal.isBlank()
                ? allowlistExtraFinal.replace("\\", "").split(",")
                : new String[0];
        return Lazy.supplier(() -> toMethodSet(methodArray));
    }

    public void setPluginAllowlist(PluginAllowlist pluginAllowlist) {
        this.pluginAllowlist = pluginAllowlist;
    }

    @Override
    protected Class<?> loadClass(String name) throws ClassNotFoundException {
        try {
            return super.loadClass(name);
        } catch (ClassNotFoundException e) {
            if (pluginClassLoader != null) {
                return pluginClassLoader.loadClass(name);
            }
            throw e;
        }
    }

    @EventListener
    public void onPluginFrameworkStarted(PluginFrameworkStartedEvent event) {
        pluginClassLoader = event.getPluginAccessor().getClassLoader();
        isPluginFrameworkStarted.set(true);
    }

    @Override
    protected boolean isIntrospectorEnabled() {
        return isPluginFrameworkStarted.get();
    }

    @Override
    protected boolean isAllowlistDebugMode() {
        return super.isAllowlistDebugMode() || allowlistDebugMode;
    }

    @Override
    protected boolean isAllowlistedClassPackageCached(Class<?> clazz) {
        return super.isAllowlistedClassPackageCached(clazz)
                || (pluginAllowlist != null && pluginAllowlist.isAllowlistedClassPackage(clazz));
    }

    @Override
    protected boolean isAllowlistedMethodCached(Method method) {
        return super.isAllowlistedMethodCached(method)
                || (pluginAllowlist != null && pluginAllowlist.isAllowlistedMethod(method));
    }

    @Override
    protected boolean isAllowlistedMethodInternal(Method method) {
        return super.isAllowlistedMethodInternal(method) || isAllowlistedBySysProp(method);
    }

    private boolean isAllowlistedBySysProp(Method method) {
        Set<String> methodAllowlist = sysPropAllowlistedMethodsRef.get().get(method.getDeclaringClass());
        return methodAllowlist != null && methodAllowlist.contains(toMethodStr(method));
    }

    // TODO: Replace usages with org.apache.velocity.util.introspection.SecureIntrospectorImpl#toMethodStr
    //  once Velocity dependency upgraded to 1.6.4-atlassian-38
    public static String toMethodStr(Method method) {
        return method.getName() + "("
                + Arrays.stream(method.getParameterTypes()).map(Class::getName).collect(joining(" ")) + ")";
    }
}
