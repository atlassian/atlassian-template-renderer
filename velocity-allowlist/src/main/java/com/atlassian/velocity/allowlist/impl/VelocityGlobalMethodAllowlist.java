package com.atlassian.velocity.allowlist.impl;

import java.lang.reflect.Method;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.velocity.allowlist.api.GlobalMethodAllowlist;
import com.atlassian.velocity.allowlist.uberspect.PluginAwareSecureIntrospector;

/**
 * A {@link GlobalMethodAllowlist} implementation that uses a {@link PluginAwareSecureIntrospector} to check if a method
 * is allowed to be invoked from a templating engine.
 *
 * @since 6.0.0
 */
public class VelocityGlobalMethodAllowlist implements GlobalMethodAllowlist {

    private final PluginAwareSecureIntrospector introspector;

    public VelocityGlobalMethodAllowlist(PluginAwareSecureIntrospector introspector) {
        this.introspector = introspector;
    }

    @Override
    public boolean isAllowlistedMethod(@Nullable Object target, @Nonnull Method method) {
        return introspector.checkObjectExecutePermission(target, method);
    }
}
