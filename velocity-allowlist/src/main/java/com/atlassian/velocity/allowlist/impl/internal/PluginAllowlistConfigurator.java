package com.atlassian.velocity.allowlist.impl.internal;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import javax.annotation.Nullable;

import org.apache.felix.framework.BundleWiringImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.atlassian.util.concurrent.Lazy;
import io.atlassian.util.concurrent.ResettableLazyReference;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.velocity.allowlist.api.internal.PluginAllowlist;
import com.atlassian.velocity.allowlist.descriptor.VelocityAllowlistModuleDescriptor;
import com.atlassian.velocity.allowlist.uberspect.PluginAwareSecureIntrospector;
import com.atlassian.velocity.allowlist.util.ClassExtractor;

import static java.lang.String.join;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toSet;

import static com.atlassian.spring.container.ContainerManager.getComponent;
import static com.atlassian.velocity.allowlist.uberspect.PluginAwareSecureIntrospector.toMethodStr;

/**
 * Holds, and can check against, all methods, classes and packages allowlisted for use in Velocity templates.
 *
 * @since 6.0.0
 */
public class PluginAllowlistConfigurator implements PluginAllowlist {

    private static final Logger LOG = LoggerFactory.getLogger(PluginAllowlistConfigurator.class);

    private final ClassExtractor classExtractor;
    private final Supplier<PluginAccessor> pluginAccessorRef;

    private final ResettableLazyReference<Map<Class<?>, Set<String>>> pluginAllowlistedMethods =
            Lazy.resettable(this::constructPluginMethodAllowlist);

    private final ResettableLazyReference<Set<Class<?>>> pluginAllowlistedClasses =
            Lazy.resettable(this::constructPluginClassAllowlist);

    private final ResettableLazyReference<Map<String, Set<String>>> pluginAllowlistedPackagesBySymbolicName =
            Lazy.resettable(this::constructPluginPackageAllowlist);

    /**
     * @deprecated since 6.1.0, use {@link #PluginAllowlistConfigurator(PluginAccessor)} instead.
     */
    @Deprecated(forRemoval = true)
    public PluginAllowlistConfigurator() {
        this(new ClassExtractor(), Lazy.supplier(() -> getComponent("pluginAccessor", PluginAccessor.class)));
    }

    public PluginAllowlistConfigurator(PluginAccessor pluginAccessor) {
        this(new ClassExtractor(), () -> pluginAccessor);
    }

    @VisibleForTesting
    PluginAllowlistConfigurator(ClassExtractor classExtractor, Supplier<PluginAccessor> pluginAccessorRef) {
        this.classExtractor = classExtractor;
        this.pluginAccessorRef = pluginAccessorRef;
    }

    @Override
    public void reload() {
        pluginAllowlistedMethods.reset();
        pluginAllowlistedClasses.reset();
        pluginAllowlistedPackagesBySymbolicName.reset();
    }

    @Override
    public boolean isAllowlistedClassPackage(Class<?> clazz) {
        return pluginAllowlistedClasses.get().contains(clazz) || isPackageAllowlisted(clazz);
    }

    @Override
    public boolean isAllowlistedMethod(Method method) {
        var methodAllowlist = pluginAllowlistedMethods.get().get(method.getDeclaringClass());
        return methodAllowlist != null && methodAllowlist.contains(toMethodStr(method));
    }

    private boolean isPackageAllowlisted(Class<?> clazz) {
        var classLoader = classExtractor.extractClassLoader(clazz);
        if (!(classLoader instanceof BundleWiringImpl.BundleClassLoader)) {
            // Class does not originate from a standard plugin
            return false;
        }
        var symbolicName = classExtractor.extractPluginBundle(classLoader).getSymbolicName();
        var allowedPackages = pluginAllowlistedPackagesBySymbolicName.get().get(symbolicName);
        return allowedPackages != null && isPackageMatches(classExtractor.extractPackage(clazz), allowedPackages);
    }

    private static String toPackageName(Package packag) {
        return packag != null ? packag.getName() : "";
    }

    private static boolean isPackageMatches(Package packag, Set<String> matchingPackages) {
        List<String> packageParts = Arrays.asList(toPackageName(packag).split("\\."));
        return IntStream.range(0, packageParts.size())
                .mapToObj(i -> join(".", packageParts.subList(0, i + 1)))
                .anyMatch(matchingPackages::contains);
    }

    private Map<Class<?>, Set<String>> constructPluginMethodAllowlist() {
        var allowlistModules = getModuleDescriptors();
        Map<Class<?>, Set<String>> methodAllowlist = new HashMap<>();
        for (var module : allowlistModules) {
            var moduleAllowlist = getModuleMethodAllowlist(module);
            if (moduleAllowlist == null) {
                module.disabled();
                continue;
            }
            putAllAndMerge(methodAllowlist, moduleAllowlist);
        }
        methodAllowlist
                .keySet()
                .forEach(clazz -> methodAllowlist.put(clazz, unmodifiableSet(methodAllowlist.get(clazz))));
        return unmodifiableMap(methodAllowlist);
    }

    /**
     * @return a map of classes to their allowlisted methods, or {@code null} if the module is invalid
     */
    @Nullable
    private Map<Class<?>, Set<String>> getModuleMethodAllowlist(VelocityAllowlistModuleDescriptor module) {
        Map<Class<?>, Set<String>> moduleAllowlist = new HashMap<>();
        var parser = module.getAllowlistElements();
        for (var entry : parser.getMethods().entrySet()) {
            var clazzName = entry.getKey();
            var methodNames = entry.getValue();
            Class<?> clazz;
            try {
                clazz = module.getPlugin().loadClass(clazzName, this.getClass());
            } catch (ClassNotFoundException e) {
                LOG.warn(
                        "Class {} cannot be loaded - disabling Velocity Allowlist plugin module {}}",
                        clazzName,
                        module.getCompleteKey());
                return null;
            }
            var invalidMethods = toInvalidMethods(clazz, methodNames);
            if (!invalidMethods.isEmpty()) {
                LOG.warn(
                        "Invalid method(s) {} defined for class {} - disabling Velocity Allowlist plugin module {}",
                        join(", ", invalidMethods),
                        clazz.getName(),
                        module.getCompleteKey());
                return null;
            }
            moduleAllowlist.put(clazz, methodNames);
        }
        return moduleAllowlist;
    }

    private static Collection<String> toInvalidMethods(Class<?> clazz, Set<String> methods) {
        var declMethods = Arrays.stream(clazz.getDeclaredMethods())
                .map(PluginAwareSecureIntrospector::toMethodStr)
                .collect(toSet());
        return methods.stream().filter(method -> !declMethods.contains(method)).toList();
    }

    private Set<Class<?>> constructPluginClassAllowlist() {
        var allowlistModules = getModuleDescriptors();
        Set<Class<?>> classAllowlist = new HashSet<>();
        moduleLoop:
        for (var module : allowlistModules) {
            var parser = module.getAllowlistElements();
            Set<Class<?>> moduleAllowlist = new HashSet<>();
            if (!isAtlassianModule(module)) {
                continue;
            }
            for (var className : parser.getClasses()) {
                try {
                    moduleAllowlist.add(module.getPlugin().loadClass(className, this.getClass()));
                } catch (ClassNotFoundException e) {
                    LOG.warn(
                            "Class {} cannot be loaded - disabling Velocity Allowlist plugin module {}",
                            className,
                            module.getCompleteKey());
                    module.disabled();
                    continue moduleLoop;
                }
            }
            classAllowlist.addAll(moduleAllowlist);
        }
        return unmodifiableSet(classAllowlist);
    }

    private Map<String, Set<String>> constructPluginPackageAllowlist() {
        var allowlistModules = getModuleDescriptors();
        Map<String, Set<String>> packageAllowlist = new HashMap<>();
        for (var module : allowlistModules) {
            var parser = module.getAllowlistElements();
            if (!isAtlassianModule(module)) {
                continue;
            }
            var packages = parser.getPackages();
            if (packages.isEmpty()) {
                continue;
            }
            String symbolicName;
            try {
                symbolicName = extractSymbolicNameIfP2(module.getPlugin());
            } catch (IllegalArgumentException e) {
                LOG.warn(
                        "Cannot allowlist package if plugin {} is not backed by an OSGi bundle - disabling Velocity Allowlist plugin module {}",
                        module.getPlugin().getKey(),
                        module.getCompleteKey());
                module.disabled();
                continue;
            }
            putAllAndMerge(packageAllowlist, Map.of(symbolicName, packages));
        }
        return unmodifiableMap(packageAllowlist);
    }

    /**
     * @throws IllegalArgumentException if the plugin is not backed by an OSGi bundle (P2)
     */
    private static String extractSymbolicNameIfP2(Plugin plugin) throws IllegalArgumentException {
        var classLoader = plugin.getClassLoader().toString();
        var tempName = classLoader.substring(classLoader.indexOf("bundle=") + "bundle=".length());
        var parsedParts = Arrays.stream(tempName.split("\\s+")).toList();
        if (parsedParts.size() != 2 || parsedParts.get(0).isEmpty()) {
            throw new IllegalArgumentException();
        }
        return parsedParts.get(0);
    }

    private static boolean isAtlassianModule(ModuleDescriptor<?> module) {
        return module.getPlugin().getPluginInformation().getVendorName().startsWith("Atlassian");
    }

    private static <S, T> void putAllAndMerge(Map<S, Set<T>> target, Map<S, Set<T>> source) {
        source.forEach((key, value) -> {
            target.putIfAbsent(key, new HashSet<>());
            target.get(key).addAll(value);
        });
    }

    /**
     * {@link PluginAccessor#getEnabledModuleDescriptorsByClass} is cached so we re-filter to ignore such modules and
     * avoid infinite loops.
     */
    private List<VelocityAllowlistModuleDescriptor> getModuleDescriptors() {
        return pluginAccessorRef
                .get()
                .getEnabledModuleDescriptorsByClass(VelocityAllowlistModuleDescriptor.class)
                .stream()
                .filter(AbstractModuleDescriptor::isEnabled)
                .toList();
    }
}
