package com.atlassian.velocity.allowlist.descriptor;

import com.atlassian.annotations.nullability.ParametersAreNonnullByDefault;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.velocity.allowlist.api.internal.PluginAllowlist;
import com.atlassian.velocity.allowlist.util.AllowlistElements;

import static java.util.Objects.requireNonNull;

/**
 * @since 6.0.0
 */
@ParametersAreNonnullByDefault
public final class VelocityAllowlistModuleDescriptor extends AbstractModuleDescriptor<Void> {

    private final PluginAllowlist pluginAllowlist;

    private AllowlistElements allowlistElements;

    public VelocityAllowlistModuleDescriptor(ModuleFactory moduleFactory, PluginAllowlist pluginAllowlist) {
        super(moduleFactory);
        this.pluginAllowlist = pluginAllowlist;
    }

    @Override
    public Void getModule() {
        return null;
    }

    @Override
    public void init(Plugin plugin, Element element) {
        super.init(plugin, element);
        allowlistElements = AllowlistElements.parse(element);
    }

    @Override
    public void enabled() {
        if (isEnabled()) {
            return;
        }
        requireNonNull(allowlistElements);
        super.enabled();
        pluginAllowlist.reload();
    }

    public AllowlistElements getAllowlistElements() {
        return allowlistElements;
    }

    @Override
    public void disabled() {
        super.disabled();
        pluginAllowlist.reload();
    }
}
