package com.atlassian.velocity.allowlist.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.Element;

import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toUnmodifiableSet;

/**
 * @since 6.1.0
 */
public class AllowlistElements {

    private final Set<String> classes;
    private final Set<String> packages;
    private final Map<String, Set<String>> methods;

    private AllowlistElements(Set<String> classes, Set<String> packages, Map<String, Set<String>> methods) {
        this.classes = requireNonNull(classes);
        this.packages = requireNonNull(packages);
        this.methods = requireNonNull(methods);
    }

    /**
     * @throws PluginParseException if unexpected element values encountered
     */
    public static AllowlistElements parse(Element rootElement) {
        var classes = rootElement.elements("class").stream()
                .map(Element::getText)
                .map(AllowlistElements::removeWhitespace)
                .collect(toUnmodifiableSet());
        var packages = rootElement.elements("package").stream()
                .map(Element::getText)
                .map(AllowlistElements::removeWhitespace)
                .collect(toUnmodifiableSet());
        var methods = toMap(
                rootElement.elements("method").stream().map(Element::getText).toList());

        return new AllowlistElements(classes, packages, methods);
    }

    private static Map<String, Set<String>> toMap(Collection<String> methodList) {
        Map<String, Set<String>> methods = new HashMap<>();
        for (String methodStr : methodList) {
            String[] parts = methodStr.trim().split("#");
            if (parts.length != 2) {
                throw new PluginParseException(
                        "Invalid method format, it should be in the format \"package.Class#method(qualified.paramType1 qualified.paramType2)\", provided method was: "
                                + methodStr);
            }
            String className = parts[0];
            String methodName = parts[1];
            methods.putIfAbsent(className, new HashSet<>());
            methods.get(className).add(methodName);
        }
        return unmodifiableMap(methods);
    }

    private static String removeWhitespace(String s) {
        return s.replaceAll("\\s", "");
    }

    public Set<String> getClasses() {
        return classes;
    }

    public Set<String> getPackages() {
        return packages;
    }

    public Map<String, Set<String>> getMethods() {
        return methods;
    }
}
