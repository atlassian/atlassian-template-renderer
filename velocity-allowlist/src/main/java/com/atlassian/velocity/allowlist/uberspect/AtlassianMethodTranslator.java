package com.atlassian.velocity.allowlist.uberspect;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.Function;

import org.springframework.aop.SpringProxy;
import org.springframework.aop.TargetClassAware;
import org.springframework.aop.framework.Advised;
import org.springframework.core.InfrastructureProxy;
import org.apache.velocity.util.introspection.MethodTranslatorImpl;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.util.ContextClassLoaderSettingInvocationHandler;

import static java.util.Collections.synchronizedMap;
import static org.apache.commons.lang3.ClassUtils.getAllInterfaces;
import static org.springframework.util.ClassUtils.CGLIB_CLASS_SEPARATOR;
import static org.springframework.util.ClassUtils.getMostSpecificMethod;

/**
 * For use with Velocity configuration property {@code introspector.method.translator} in products where any of the
 * following proxies are present:
 * <ul>
 *     <li>HibernateProxy</li>
 *     <li>Spring InfrastructureProxy</li>
 *     <li>Spring TargetClassAware based proxy</li>
 *     <li>ContextClassLoaderSettingInvocationHandler backed OSGi service dynamic proxy</li>
 *     <li>AnnotationPreservingInvocationHandler backed HtmlSafe dynamic proxy</li>
 *     <li>HostComponentFactoryBean$DynamicServiceInvocationHandler backed OSGi component dynamic proxy</li>
 * </ul>
 * It additionally rectifies cases where the method resolved from the engine is an interface method, by finding the most
 * specific method in the target class.
 *
 * @since 6.0.0
 */
public class AtlassianMethodTranslator extends MethodTranslatorImpl {

    private static final Logger log = LoggerFactory.getLogger(AtlassianMethodTranslator.class);

    private final Map<Method, Method> unproxyMethodCache = synchronizedMap(new WeakHashMap<>());

    private final Field cclTargetField;

    // Class is package-private, related fields initialised using reflection
    public static final String HTMLSAFE_HANDLER_NAME =
            "com.atlassian.velocity.htmlsafe.introspection.AnnotationPreservingInvocationHandler";
    private final Class<InvocationHandler> htmlSafeHandlerClass;
    private final Field htmlSafeTargetField;

    // Class is package-private and not part of system bundle, related fields lazily initialised
    public static final String OSGI_COMPONENT_HANDLER_NAME =
            "com.atlassian.plugin.osgi.bridge.external.HostComponentFactoryBean$DynamicServiceInvocationHandler";
    private Class<InvocationHandler> osgiComponentHandlerClass;
    private Field osgiComponentTargetField;

    public AtlassianMethodTranslator() throws Exception {
        htmlSafeHandlerClass = (Class<InvocationHandler>) Class.forName(HTMLSAFE_HANDLER_NAME);
        cclTargetField = ContextClassLoaderSettingInvocationHandler.class.getDeclaredField("service");
        htmlSafeTargetField = htmlSafeHandlerClass.getDeclaredField("targetObject");
        cclTargetField.setAccessible(true);
        htmlSafeTargetField.setAccessible(true);
    }

    /**
     * The OSGi component handler class is not part of the system bundle, so we need to lazily initialise the related
     * fields when the handler is first encountered.
     */
    private void initialiseOsgiComponentFields(Class<?> osgiComponentHandlerClass) {
        this.osgiComponentHandlerClass = (Class<InvocationHandler>) osgiComponentHandlerClass;
        try {
            this.osgiComponentTargetField = osgiComponentHandlerClass.getDeclaredField("service");
            this.osgiComponentTargetField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Method getTranslatedMethod(Object target, Method method) {
        // Translates proxy or abstract methods to their most specific implementation
        if (!isProxy(target.getClass()) && !Modifier.isAbstract(method.getModifiers())) {
            return method;
        }
        return getUnproxiedSpecificMethodCached(target, method);
    }

    protected Method getUnproxiedSpecificMethodCached(Object target, Method method) {
        return unproxyMethodCache.computeIfAbsent(method, key -> getUnproxiedSpecificMethod(target, key));
    }

    protected Method getUnproxiedSpecificMethod(Object target, Method method) {
        if (!isProxy(target.getClass())) {
            return getMostSpecificMethod(method, target.getClass());
        }

        for (var unproxyingFunction : unproxyingFunctions()) {
            Object newTarget = unproxyingFunction.apply(target);
            if (newTarget != target && newTarget != null) {
                // Recurse in case of arbitrary number of nested proxies
                return getUnproxiedSpecificMethodCached(newTarget, method);
            }
        }

        Class<?> newTargetClass = classUnproxyingFunction(target);
        if (!isProxy(newTargetClass)) {
            return getMostSpecificMethod(method, newTargetClass);
        }

        Method interfaceMethod = super.getTranslatedMethod(target, method);
        log.debug(
                "Unproxying failed{}, falling back to interface method: {}",
                Proxy.isProxyClass(target.getClass())
                        ? " for dynamic proxy with handler %s"
                                .formatted(Proxy.getInvocationHandler(target).getClass())
                        : "",
                interfaceMethod);
        return interfaceMethod;
    }

    /**
     * Can be overridden to add more unproxying functions, perhaps product-specific.
     */
    protected List<Function<Object, ?>> unproxyingFunctions() {
        return List.of(
                this::getHibernateProxyTarget,
                this::getSpringAdvisedProxyTarget,
                this::getSpringInfraProxyTarget,
                this::getContextClassLoaderProxyTarget,
                this::getHtmlSafeProxyTarget,
                this::getOsgiComponentProxyTarget);
    }

    /**
     * An unproxying function that can only resolve the target class rather than the instance.
     */
    protected Class<?> classUnproxyingFunction(Object target) {
        Class<?> result = null;
        if (isImplementsInterfaceReflective(target, TargetClassAware.class)) {
            result = (Class<?>) reflectiveMethodCallNoArgs(target, "getTargetClass");
        }
        if (result == null) {
            result = isCglibProxy(target) ? target.getClass().getSuperclass() : target.getClass();
        }
        return result;
    }

    protected Object getHibernateProxyTarget(Object proxy) {
        try {
            if (proxy instanceof HibernateProxy) {
                return Hibernate.unproxy(proxy);
            }
        } catch (LinkageError e) {
            // Silent skip bytecode error if Hibernate not on classpath
        }
        return proxy;
    }

    protected Object getSpringAdvisedProxyTarget(Object proxy) {
        if (isImplementsInterfaceReflective(proxy, Advised.class)) {
            Object targetSource = reflectiveMethodCallNoArgs(proxy, "getTargetSource");
            try {
                return reflectiveMethodCallNoArgs(targetSource, "getTarget");
            } catch (Exception e) {
                return proxy;
            }
        }
        return proxy;
    }

    protected Object getSpringInfraProxyTarget(Object proxy) {
        if (isImplementsInterfaceReflective(proxy, InfrastructureProxy.class)) {
            return reflectiveMethodCallNoArgs(proxy, "getWrappedObject");
        }
        return proxy;
    }

    protected Object getContextClassLoaderProxyTarget(Object proxy) {
        if (Proxy.isProxyClass(proxy.getClass())
                && Proxy.getInvocationHandler(proxy)
                        instanceof ContextClassLoaderSettingInvocationHandler invocationHandler) {
            return getService(invocationHandler);
        }
        return proxy;
    }

    protected Object getHtmlSafeProxyTarget(Object proxy) {
        if (Proxy.isProxyClass(proxy.getClass())
                && Proxy.getInvocationHandler(proxy).getClass().equals(htmlSafeHandlerClass)) {
            return getHtmlSafeTarget(Proxy.getInvocationHandler(proxy));
        }
        return proxy;
    }

    protected Object getOsgiComponentProxyTarget(Object proxy) {
        if (Proxy.isProxyClass(proxy.getClass())) {
            var invocationHandler = Proxy.getInvocationHandler(proxy);
            var handlerClass = invocationHandler.getClass();
            if (osgiComponentHandlerClass != null && osgiComponentHandlerClass.equals(handlerClass)) {
                return getOsgiComponentTarget(invocationHandler);
            } else if (handlerClass.getName().equals(OSGI_COMPONENT_HANDLER_NAME)) {
                initialiseOsgiComponentFields(handlerClass);
                return getOsgiComponentTarget(invocationHandler);
            }
        }
        return proxy;
    }

    /**
     * Unfortunately, Confluence and potentially other products have 2 copies of most Spring classes, one that is part
     * of the system bundle and used by internal beans, and another which is exported as part of the
     * {@code platform-spring-bundle} and utilised by other beans. We need to cover both scenarios, so we use a
     * reflective approach which only compares the fully qualified class names.
     */
    private static boolean isImplementsInterfaceReflective(Object target, Class<?> iface) {
        return target != null
                && getAllInterfaces(target.getClass()).stream()
                        .map(Class::getName)
                        .anyMatch(iface.getName()::equals);
    }

    /**
     * It follows that we also need to use reflection to call methods on these interfaces.
     */
    private static Object reflectiveMethodCallNoArgs(Object target, String methodName) {
        try {
            Method method = target.getClass().getMethod(methodName);
            return method.invoke(target);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Reflective variant of {@link org.springframework.aop.support.AopUtils#isCglibProxy(Object)}
     */
    private static boolean isCglibProxy(Object object) {
        return isImplementsInterfaceReflective(object, SpringProxy.class)
                && object.getClass().getName().contains(CGLIB_CLASS_SEPARATOR);
    }

    /**
     * Retrieves the target of a {@link ContextClassLoaderSettingInvocationHandler} proxy by accessing the private
     * {@code service} field.
     */
    private Object getService(ContextClassLoaderSettingInvocationHandler handler) {
        try {
            return cclTargetField.get(handler);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Retrieves the target of an {@code AnnotationPreservingInvocationHandler} proxy by accessing the private
     * {@code targetObject} field.
     */
    private Object getHtmlSafeTarget(InvocationHandler handler) {
        try {
            return htmlSafeTargetField.get(handler);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Retrieves the target of an {@code HostComponentFactoryBean$DynamicServiceInvocationHandler} proxy by accessing
     * the private {@code service} field.
     */
    private Object getOsgiComponentTarget(InvocationHandler handler) {
        try {
            return osgiComponentTargetField.get(handler);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isProxy(Class<?> clazz) {
        return clazz.getSimpleName().contains("$");
    }
}
