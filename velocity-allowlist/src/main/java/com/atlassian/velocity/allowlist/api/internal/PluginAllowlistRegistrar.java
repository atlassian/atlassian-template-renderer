package com.atlassian.velocity.allowlist.api.internal;

import java.util.Map;
import java.util.Set;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.plugin.Plugin;

/**
 * Holds a register of classes and packages marked as allowlisted by plugins.
 *
 * @since 6.0.0
 * @deprecated since 6.1.0, use {@link PluginAllowlist#reload} instead.
 */
@Deprecated(forRemoval = true)
public interface PluginAllowlistRegistrar extends PluginAllowlist {

    /**
     * @deprecated since 6.1.0, no replacement.
     */
    @Deprecated(forRemoval = true)
    default void registerPluginMethods(
            Plugin plugin, ModuleCompleteKey moduleCompleteKey, Map<String, Set<String>> classMethods) {
        reload();
    }

    /**
     * @deprecated since 6.1.0, no replacement.
     */
    @Deprecated(forRemoval = true)
    default void registerPluginClasses(Plugin plugin, ModuleCompleteKey moduleCompleteKey, Set<String> classes) {
        reload();
    }

    /**
     * @deprecated since 6.1.0, no replacement.
     */
    @Deprecated(forRemoval = true)
    default void registerPluginPackages(Plugin plugin, ModuleCompleteKey moduleCompleteKey, Set<String> packages) {
        reload();
    }

    /**
     * @deprecated since 6.1.0, no replacement.
     */
    @Deprecated(forRemoval = true)
    default void clearPluginAllowlist(ModuleCompleteKey moduleCompleteKey) {
        reload();
    }
}
