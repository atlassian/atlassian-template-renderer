package com.atlassian.velocity.allowlist.util;

import org.apache.felix.framework.BundleWiringImpl;
import org.osgi.framework.Bundle;

/**
 * Exists to enable unit testing of methods which can not usually be mocked
 *
 * @since 6.0.0
 */
public class ClassExtractor {

    public Bundle extractPluginBundle(ClassLoader classLoader) {
        return ((BundleWiringImpl.BundleClassLoader) classLoader).getBundle();
    }

    public ClassLoader extractClassLoader(Class<?> clazz) {
        return clazz.getClassLoader();
    }

    public Package extractPackage(Class<?> clazz) {
        return clazz.getPackage();
    }
}
