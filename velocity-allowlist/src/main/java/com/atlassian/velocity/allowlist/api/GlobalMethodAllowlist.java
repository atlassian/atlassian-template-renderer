package com.atlassian.velocity.allowlist.api;

import java.lang.reflect.Method;
import javax.annotation.Nullable;

/**
 * Generic method allowlist that can be used to check if a method is allowed to be invoked from a templating engine or
 * related use cases.
 *
 * @since 6.0.0
 */
public interface GlobalMethodAllowlist {

    default boolean isAllowlistedMethod(Method method) {
        return isAllowlistedMethod(null, method);
    }

    boolean isAllowlistedMethod(@Nullable Object target, Method method);
}
