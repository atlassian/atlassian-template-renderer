package com.atlassian.velocity.allowlist.impl.internal;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.felix.framework.BundleWiringImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.osgi.framework.Bundle;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.atlassian.velocity.allowlist.descriptor.VelocityAllowlistModuleDescriptor;
import com.atlassian.velocity.allowlist.util.AllowlistElements;
import com.atlassian.velocity.allowlist.util.ClassExtractor;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.velocity.allowlist.uberspect.PluginAwareSecureIntrospector.toMethodStr;

public class PluginAllowlistConfiguratorTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ClassExtractor classExtractor;

    @Mock
    private PluginAccessor pluginAccessor;

    private PluginAllowlistConfigurator configurator;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private Plugin plugin1;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private Plugin plugin2;

    private String bundleSymbolicName1 = "com.plugin.symbolic-name.one";

    private String bundleSymbolicName2 = "com.plugin.symbolic-name.two";

    @Mock
    private VelocityAllowlistModuleDescriptor descriptor1;

    @Mock
    private VelocityAllowlistModuleDescriptor descriptor2;

    @Mock
    private AllowlistElements elements1;

    @Mock
    private AllowlistElements elements2;

    private Class<?> testClass = Object.class;
    private Method testClassMethod = testClass.getMethod("toString");
    private Method testClassMethod2 = testClass.getMethod("hashCode");

    private Class<?> testClass2 = String.class;
    private Method testClass2Method = testClass2.getMethod("toString");
    private Method testClass2Method2 = testClass2.getMethod("hashCode");

    public PluginAllowlistConfiguratorTest() throws Exception {}

    @Before
    public void setUp() throws Exception {
        configurator = new PluginAllowlistConfigurator(classExtractor, () -> pluginAccessor);

        when(classExtractor.extractPackage(any())).thenCallRealMethod();

        mockAtlassianPlugin(plugin1);
        mockAtlassianPlugin(plugin2);

        when(descriptor1.getPlugin()).thenReturn(plugin1);
        when(descriptor2.getPlugin()).thenReturn(plugin2);
        when(descriptor1.isEnabled()).thenReturn(true);
        when(descriptor2.isEnabled()).thenReturn(true);
        when(descriptor1.getAllowlistElements()).thenReturn(elements1);
        when(descriptor2.getAllowlistElements()).thenReturn(elements2);
        when(pluginAccessor.getEnabledModuleDescriptorsByClass(VelocityAllowlistModuleDescriptor.class))
                .thenReturn(List.of(descriptor1, descriptor2));
    }

    private void mockAtlassianPlugin(Plugin plugin) throws Exception {
        doAnswer(invocation -> {
                    String clazz = invocation.getArgument(0);
                    Class<?> callingClass = invocation.getArgument(1);
                    return ClassLoaderUtils.loadClass(clazz, callingClass);
                })
                .when(plugin)
                .loadClass(any(), any());
        when(plugin.getPluginInformation().getVendorName()).thenReturn("Atlassian Pty Ltd");
    }

    private void mockPlugin1OriginatingClass(Class<?> clazz) {
        mockPluginOriginatingClass(clazz, plugin1, bundleSymbolicName1);
    }

    private void mockPlugin2OriginatingClass(Class<?> clazz) {
        mockPluginOriginatingClass(clazz, plugin2, bundleSymbolicName2);
    }

    /**
     * Necessary for testing sandboxing of package allowlisting configuration between plugins.
     */
    private void mockPluginOriginatingClass(Class<?> clazz, Plugin plugin, String bundleSymbolicName) {
        var bundleClassLoader = mock(BundleWiringImpl.BundleClassLoader.class);
        when(plugin.getClassLoader()).thenReturn(bundleClassLoader);
        when(bundleClassLoader.toString())
                .thenReturn("BundleClassLoader@6819598[bundle=%s [298]]".formatted(bundleSymbolicName));
        when(classExtractor.extractClassLoader(clazz)).thenReturn(bundleClassLoader);
        var bundle = mock(Bundle.class);
        when(classExtractor.extractPluginBundle(bundleClassLoader)).thenReturn(bundle);
        when(bundle.getSymbolicName()).thenReturn(bundleSymbolicName);
    }

    /**
     * Allowlisting configuration is lazily computed and reset when #reload is called.
     */
    @Test
    public void reload() {
        // Given configuration is loaded (lazily)
        mockPlugin1OriginatingClass(testClass); // Necessary to trigger package allowlist
        configurator.isAllowlisted(testClassMethod);
        clearInvocations(pluginAccessor);

        // When #reload called and configuration needed again
        configurator.reload();
        configurator.isAllowlisted(testClassMethod);

        // Then expect, once for each type of allowlisting: method, class package
        verify(pluginAccessor, times(3)).getEnabledModuleDescriptorsByClass(VelocityAllowlistModuleDescriptor.class);
    }

    @Test
    public void isAllowlistedClassPackage_class() {
        when(elements1.getClasses()).thenReturn(Set.of(testClass.getName()));

        assertTrue(configurator.isAllowlistedClassPackage(testClass));
        assertFalse(configurator.isAllowlistedClassPackage(testClass2));
    }

    @Test
    public void isAllowlistedClassPackage_class_alternate() {
        when(elements2.getClasses()).thenReturn(Set.of(testClass2.getName()));

        assertTrue(configurator.isAllowlistedClassPackage(testClass2));
        assertFalse(configurator.isAllowlistedClassPackage(testClass));
    }

    @Test
    public void isAllowlistedClassPackage_class_alternate2() {
        when(elements1.getClasses()).thenReturn(Set.of(testClass2.getName()));

        assertTrue(configurator.isAllowlistedClassPackage(testClass2));
        assertFalse(configurator.isAllowlistedClassPackage(testClass));
    }

    /**
     * {@link PluginAccessor#getEnabledModuleDescriptorsByClass} may sometimes return disabled modules, ensure we ignore
     * such modules and avoid infinite loops.
     */
    @Test
    public void isAllowlistedClassPackage_class_disabledModule() {
        when(descriptor1.isEnabled()).thenReturn(false);
        lenient().when(elements1.getClasses()).thenReturn(Set.of(testClass.getName()));

        assertFalse(configurator.isAllowlistedClassPackage(testClass));
    }

    /**
     * Class allowlisting is not supported by non-Atlassian plugins.
     */
    @Test
    public void isAllowlistedClassPackage_class_nonAtlassian() {
        when(elements1.getClasses()).thenReturn(Set.of(testClass.getName()));
        when(plugin1.getPluginInformation().getVendorName()).thenReturn("Not Atlassian");

        assertFalse(configurator.isAllowlistedClassPackage(testClass));
    }

    /**
     * Package allowlisting is sandboxed between plugins. That is, a package allowlisted in one plugin only applies to
     * classes that also originate from that plugin.
     */
    @Test
    public void isAllowlistedClassPackage_package() {
        mockPlugin1OriginatingClass(testClass); // plugin1 corresponds to elements1
        when(elements1.getPackages()).thenReturn(Set.of(testClass.getPackage().getName()));

        assertTrue(configurator.isAllowlistedClassPackage(testClass));
        assertFalse(configurator.isAllowlistedClassPackage(testClass2));
    }

    @Test
    public void isAllowlistedClassPackage_package_alternate() {
        mockPlugin2OriginatingClass(testClass); // plugin2 corresponds to elements2
        when(elements2.getPackages()).thenReturn(Set.of(testClass.getPackage().getName()));

        assertTrue(configurator.isAllowlistedClassPackage(testClass));
        assertFalse(configurator.isAllowlistedClassPackage(testClass2));
    }

    @Test
    public void isAllowlistedClassPackage_package_alternate2() {
        mockPlugin1OriginatingClass(testClass2); // plugin1 corresponds to elements1
        when(elements1.getPackages()).thenReturn(Set.of(testClass2.getPackage().getName()));

        assertTrue(configurator.isAllowlistedClassPackage(testClass2));
        assertFalse(configurator.isAllowlistedClassPackage(testClass));
    }

    /**
     * Since package allowlisting is sandboxed between plugins, we expect this allowlisting entry to be ignored.
     */
    @Test
    public void isAllowlistedClassPackage_package_differentPlugin() {
        mockPlugin2OriginatingClass(testClass); // plugin2 does not correspond to elements1
        when(elements1.getPackages()).thenReturn(Set.of(testClass.getPackage().getName()));

        assertFalse(configurator.isAllowlistedClassPackage(testClass));
        assertFalse(configurator.isAllowlistedClassPackage(testClass2));
    }

    @Test
    public void isAllowlistedClassPackage_package_differentPlugin_alternate() {
        mockPlugin1OriginatingClass(testClass); // plugin1 does not correspond to elements2
        when(elements2.getPackages()).thenReturn(Set.of(testClass.getPackage().getName()));

        assertFalse(configurator.isAllowlistedClassPackage(testClass));
        assertFalse(configurator.isAllowlistedClassPackage(testClass2));
    }

    @Test
    public void isAllowlistedClassPackage_package_differentPlugin_alternate2() {
        mockPlugin1OriginatingClass(testClass2); // plugin1 does not correspond to elements2
        when(elements2.getPackages()).thenReturn(Set.of(testClass2.getPackage().getName()));

        assertFalse(configurator.isAllowlistedClassPackage(testClass));
        assertFalse(configurator.isAllowlistedClassPackage(testClass2));
    }

    /**
     * Package allowlisting is not supported by non-Atlassian plugins.
     */
    @Test
    public void isAllowlistedClassPackage_package_nonAtlassian() {
        when(plugin1.getPluginInformation().getVendorName()).thenReturn("Not Atlassian");
        mockPlugin1OriginatingClass(testClass); // plugin1 corresponds to elements1
        when(elements1.getPackages()).thenReturn(Set.of(testClass.getPackage().getName()));

        assertFalse(configurator.isAllowlistedClassPackage(testClass));
    }

    @Test
    public void isAllowlistedMethod() {
        when(elements1.getMethods()).thenReturn(Map.of(testClass.getName(), Set.of(toMethodStr(testClassMethod))));
        when(elements2.getMethods()).thenReturn(Map.of(testClass2.getName(), Set.of(toMethodStr(testClass2Method2))));

        assertTrue(configurator.isAllowlistedMethod(testClassMethod));
        assertFalse(configurator.isAllowlistedMethod(testClassMethod2));
        assertFalse(configurator.isAllowlistedMethod(testClass2Method));
        assertTrue(configurator.isAllowlistedMethod(testClass2Method2));
    }

    @Test
    public void isAllowlistedMethod_alternate() {
        when(elements2.getMethods())
                .thenReturn(Map.of(
                        testClass.getName(), Set.of(toMethodStr(testClassMethod), toMethodStr(testClassMethod2))));

        assertTrue(configurator.isAllowlistedMethod(testClassMethod));
        assertTrue(configurator.isAllowlistedMethod(testClassMethod2));
        assertFalse(configurator.isAllowlistedMethod(testClass2Method));
        assertFalse(configurator.isAllowlistedMethod(testClass2Method2));
    }
}
