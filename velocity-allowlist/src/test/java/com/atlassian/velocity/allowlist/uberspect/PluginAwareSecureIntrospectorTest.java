package com.atlassian.velocity.allowlist.uberspect;

import java.lang.reflect.Method;

import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.RestoreSystemProperties;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.event.events.PluginFrameworkStartedEvent;
import com.atlassian.velocity.allowlist.api.internal.PluginAllowlist;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import static com.atlassian.velocity.allowlist.uberspect.PluginAwareSecureIntrospector.toMethodStr;

public class PluginAwareSecureIntrospectorTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public RestoreSystemProperties restoreSystemProperties = new RestoreSystemProperties();

    @Mock(answer = RETURNS_DEEP_STUBS)
    private RuntimeServices runtimeServices;

    private final Log log = new Log();

    @Mock
    private ClassLoader pluginClassLoader;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private PluginFrameworkStartedEvent event;

    @Mock
    private PluginAllowlist pluginAllowlist;

    private PluginAwareSecureIntrospector secureIntrospector;

    @Before
    public void setUp() throws Exception {
        when(event.getPluginAccessor().getClassLoader()).thenReturn(pluginClassLoader);
        doReturn(String.class).when(pluginClassLoader).loadClass("made.up.Class");

        when(runtimeServices.getConfiguration().getString(eq(RuntimeConstants.INTROSPECTOR_CACHE_CLASS), anyString()))
                .thenReturn(org.apache.velocity.util.introspection.IntrospectorCacheImpl.class.getName());
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[] {});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES))
                .thenReturn(new String[] {});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_CLASSES))
                .thenReturn(new String[] {});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_CLASSES))
                .thenReturn(new String[] {});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_PACKAGES))
                .thenReturn(new String[] {});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_METHODS))
                .thenReturn(new String[] {});
        when(runtimeServices
                        .getConfiguration()
                        .getBoolean(eq(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE), anyBoolean()))
                .thenReturn(true);
        when(runtimeServices
                        .getConfiguration()
                        .getBoolean(eq(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_DEBUG), anyBoolean()))
                .thenReturn(false);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_METHODS))
                .thenReturn(new String[] {});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_CLASSES))
                .thenReturn(new String[] {});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_PACKAGES))
                .thenReturn(new String[] {});
        when(runtimeServices
                        .getConfiguration()
                        .getString(eq(RuntimeConstants.INTROSPECTOR_METHOD_TRANSLATOR), anyString()))
                .thenReturn(AtlassianMethodTranslator.class.getName());

        initIntrospector();
    }

    private void initIntrospector() {
        secureIntrospector = new PluginAwareSecureIntrospector(log, runtimeServices);
        secureIntrospector.onPluginFrameworkStarted(event);
    }

    @Test
    public void onPluginFrameworkStarted() throws Exception {
        // Reconstruct the introspector to test the initial state
        secureIntrospector = new PluginAwareSecureIntrospector(log, runtimeServices);

        assertFalse(secureIntrospector.isIntrospectorEnabled());
        assertThrows(ClassNotFoundException.class, () -> secureIntrospector.loadClass("made.up.Class"));

        secureIntrospector.onPluginFrameworkStarted(event);

        assertTrue(secureIntrospector.isIntrospectorEnabled());
        assertEquals(String.class, secureIntrospector.loadClass("made.up.Class"));
    }

    @Test
    public void isAllowlistedClassPackage() throws Exception {
        Method method = Object.class.getMethod("hashCode");

        assertTrue(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));

        secureIntrospector.setPluginAllowlist(pluginAllowlist);
        when(pluginAllowlist.isAllowlistedClassPackage(Object.class)).thenReturn(true);

        assertFalse(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));
    }

    @Test
    public void isAllowlistedMethod() throws Exception {
        Method method = Object.class.getMethod("hashCode");

        assertTrue(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));

        secureIntrospector.setPluginAllowlist(pluginAllowlist);
        when(pluginAllowlist.isAllowlistedMethod(method)).thenReturn(true);

        assertFalse(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));
    }

    @Test
    public void debugMode() {
        assertFalse(secureIntrospector.isAllowlistDebugMode());

        System.setProperty(PluginAwareSecureIntrospector.ALLOWLIST_DEBUG_PROPERTY, "true");
        secureIntrospector = new PluginAwareSecureIntrospector(log, runtimeServices);

        assertTrue(secureIntrospector.isAllowlistDebugMode());
    }

    @Test
    public void allowlistWithSysProp() throws Exception {
        Method method = Object.class.getMethod("hashCode");

        assertTrue(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));

        System.setProperty(
                PluginAwareSecureIntrospector.ALLOWLIST_EXTRA_PROPERTY,
                method.getDeclaringClass().getName() + "#" + toMethodStr(method));
        initIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));
    }

    /**
     * Superfluous backslashes which may arise from improper escaping of system property value should be disregarded.
     */
    @Test
    public void allowlistWithSysProp_escapingBackslash() throws Exception {
        Method method = Object.class.getMethod("hashCode");

        assertTrue(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));

        System.setProperty(
                PluginAwareSecureIntrospector.ALLOWLIST_EXTRA_PROPERTY,
                method.getDeclaringClass().getName() + "#" + "\\" + toMethodStr(method));
        initIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));
    }

    @Test
    public void allowlistWithSysProp_multipleEntries() throws Exception {
        Method method = Object.class.getMethod("hashCode");
        Method method2 = Object.class.getMethod("toString");

        System.setProperty(
                PluginAwareSecureIntrospector.ALLOWLIST_EXTRA_PROPERTY,
                method.getDeclaringClass().getName() + "#" + toMethodStr(method) + ", "
                        + method2.getDeclaringClass().getName() + "#" + toMethodStr(method2));
        initIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));
        assertFalse(secureIntrospector.isExecutionRestricted(null, method2, new Object[0]));
    }

    @Test
    public void allowlistWithSysProp_multipleProps() throws Exception {
        Method method = Object.class.getMethod("hashCode");
        Method method2 = Object.class.getMethod("toString");

        System.setProperty(
                PluginAwareSecureIntrospector.ALLOWLIST_EXTRA_PROPERTY,
                method.getDeclaringClass().getName() + "#" + toMethodStr(method));
        System.setProperty(
                PluginAwareSecureIntrospector.ALLOWLIST_EXTRA_PROPERTY_ALT,
                method2.getDeclaringClass().getName() + "#" + toMethodStr(method2));
        initIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(null, method, new Object[0]));
        assertFalse(secureIntrospector.isExecutionRestricted(null, method2, new Object[0]));
    }
}
