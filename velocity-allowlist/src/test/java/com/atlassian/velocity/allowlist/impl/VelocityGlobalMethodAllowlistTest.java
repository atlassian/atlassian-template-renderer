package com.atlassian.velocity.allowlist.impl;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.velocity.allowlist.api.GlobalMethodAllowlist;
import com.atlassian.velocity.allowlist.uberspect.PluginAwareSecureIntrospector;

import static org.mockito.Mockito.verify;

public class VelocityGlobalMethodAllowlistTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private PluginAwareSecureIntrospector introspector;

    private GlobalMethodAllowlist globalMethodAllowlist;

    private Object target;

    private Method method;

    @Before
    public void setUp() throws Exception {
        globalMethodAllowlist = new VelocityGlobalMethodAllowlist(introspector);
        target = new Object();
        method = Object.class.getMethod("toString");
    }

    @Test
    public void isAllowlistedMethod_withTarget() {
        globalMethodAllowlist.isAllowlistedMethod(target, method);

        verify(introspector).checkObjectExecutePermission(target, method);
    }

    @Test
    public void isAllowlistedMethod() {
        globalMethodAllowlist.isAllowlistedMethod(method);

        verify(introspector).checkObjectExecutePermission(null, method);
    }
}
