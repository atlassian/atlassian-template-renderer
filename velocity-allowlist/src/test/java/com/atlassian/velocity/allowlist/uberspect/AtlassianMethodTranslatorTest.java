package com.atlassian.velocity.allowlist.uberspect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Set;

import org.springframework.aop.TargetClassAware;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.target.SingletonTargetSource;
import org.springframework.core.InfrastructureProxy;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import com.atlassian.plugin.util.ContextClassLoaderSettingInvocationHandler;
import com.atlassian.velocity.htmlsafe.introspection.AnnotatedValue;
import com.atlassian.velocity.htmlsafe.introspection.AnnotationBoxedElement;

import static java.util.Collections.emptyList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.nullable;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.velocity.allowlist.uberspect.AtlassianMethodTranslator.HTMLSAFE_HANDLER_NAME;
import static com.atlassian.velocity.allowlist.uberspect.AtlassianMethodTranslator.OSGI_COMPONENT_HANDLER_NAME;

public class AtlassianMethodTranslatorTest {

    private AtlassianMethodTranslator translator;
    private DummyInterface originalObject;
    private Method objectMethod;

    private final Class<InvocationHandler> HTML_SAFE_HANDLER_CLASS =
            (Class<InvocationHandler>) Class.forName(HTMLSAFE_HANDLER_NAME);
    private final Class<InvocationHandler> OSGI_COMPONENT_HANDLER_CLASS =
            (Class<InvocationHandler>) Class.forName(OSGI_COMPONENT_HANDLER_NAME);

    public AtlassianMethodTranslatorTest() throws Exception {}

    @Before
    public void setUp() throws Exception {
        translator = new AtlassianMethodTranslator();
        originalObject = new DummyObject();
        objectMethod = DummyObject.class.getMethod("method");
    }

    @Test
    public void ensureSpringProxyTranslated() throws Exception {
        DummyInterface proxyObject = mockSpringProxy(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureHibernateProxyTranslated() throws Exception {
        DummyInterface proxyObject = mockHibernateProxy(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureSpringAdvisedProxyTranslated() throws Exception {
        DummyInterface proxyObject = mockSpringAdvisedProxy(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureSpringInfraProxyTranslated() throws Exception {
        DummyInterface proxyObject = mockSpringInfraProxy(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureCCLProxyTranslated() throws Exception {
        DummyInterface proxyObject = mockCCLProxy(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureHtmlSafeProxyTranslated() throws Exception {
        DummyInterface proxyObject = mockHtmlSafeProxy(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureOsgiComponentProxyTranslated() throws Exception {
        DummyInterface proxyObject = mockOsgiComponentProxy(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    /**
     * OSGi services which are imported into a plugin using Spring Java Config usually exhibit 2 levels of proxying.
     * This unit test mocks this scenario and ensures that the underlying concrete method is resolved correctly.
     */
    @Test
    public void ensureSpringOsgiServiceTranslated() throws Exception {
        DummyInterface proxyObject = mockSpringOsgiService(originalObject, DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureMultiLevelNestedProxiesTranslated() throws Exception {
        DummyInterface proxyObject = mockHibernateProxy(
                mockSpringInfraProxy(
                        mockCCLProxy(
                                mockHtmlSafeProxy(
                                        mockOsgiComponentProxy(originalObject, DummyInterface.class),
                                        DummyInterface.class),
                                DummyInterface.class),
                        DummyInterface.class),
                DummyInterface.class);
        Method proxyMethod = proxyObject.getClass().getMethod("method");

        assertEquals(objectMethod, translator.getTranslatedMethod(proxyObject, proxyMethod));
    }

    @Test
    public void ensureNonProxiedMethodReturnsItself() {
        assertEquals(objectMethod, translator.getTranslatedMethod(originalObject, objectMethod));
    }

    /**
     * The Velocity engine sometimes resolves an interface method instead of the concrete method. This is
     * inconsequential for invoking the method but is not correct for the purpose of the allowlist. This unit test
     * ensures the translator correctly resolves the concrete method in these cases.
     */
    @Test
    public void ensureAbstractMethodTranslated() throws NoSuchMethodException {
        assertEquals(
                objectMethod, translator.getTranslatedMethod(originalObject, DummyInterface.class.getMethod("method")));
    }

    static final class DummyObject implements DummyInterface {
        @Override
        public void method() {}
    }

    interface DummyInterface {
        void method();
    }

    @SuppressWarnings("unchecked")
    <T> T mockSpringProxy(T originalObject, Class<T> proxyInterface) {
        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] {proxyInterface, TargetClassAware.class},
                new DummySpringProxyHandler(originalObject));
    }

    static class DummySpringProxyHandler implements InvocationHandler {
        private final Object instance;

        public DummySpringProxyHandler(Object instance) {
            this.instance = instance;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (TargetClassAware.class.getMethod("getTargetClass").equals(method)) {
                return instance.getClass();
            }
            return method.invoke(instance, args);
        }
    }

    @SuppressWarnings("unchecked")
    <T> T mockSpringAdvisedProxy(T originalObject, Class<T> proxyInterface) {
        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] {proxyInterface, Advised.class},
                new DummySpringAdvisedProxyHandler(originalObject));
    }

    static class DummySpringAdvisedProxyHandler implements InvocationHandler {
        private final Object instance;

        public DummySpringAdvisedProxyHandler(Object instance) {
            this.instance = instance;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (TargetClassAware.class.getMethod("getTargetClass").equals(method)) {
                return instance.getClass();
            }
            if (Advised.class.getMethod("getTargetSource").equals(method)) {
                var targetSource = mock(SingletonTargetSource.class);
                when(targetSource.getTarget()).thenReturn(instance);
                return targetSource;
            }
            return method.invoke(instance, args);
        }
    }

    @SuppressWarnings("unchecked")
    <T> T mockHibernateProxy(T originalObject, Class<T> proxyInterface) {
        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] {proxyInterface, HibernateProxy.class},
                new DummyHibernateProxyHandler(originalObject));
    }

    static class DummyHibernateProxyHandler implements InvocationHandler {
        private final Object instance;

        public DummyHibernateProxyHandler(Object instance) {
            this.instance = instance;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (HibernateProxy.class.getMethod("getHibernateLazyInitializer").equals(method)) {
                var initializer = mock(LazyInitializer.class);
                when(initializer.getImplementation()).thenReturn(instance);
                return initializer;
            }
            return method.invoke(instance, args);
        }
    }

    @SuppressWarnings("unchecked")
    <T> T mockSpringInfraProxy(T originalObject, Class<T> proxyInterface) {
        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] {proxyInterface, InfrastructureProxy.class},
                new DummySpringInfraProxyHandler(originalObject));
    }

    static class DummySpringInfraProxyHandler implements InvocationHandler {
        private final Object instance;

        public DummySpringInfraProxyHandler(Object instance) {
            this.instance = instance;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (InfrastructureProxy.class.getMethod("getWrappedObject").equals(method)) {
                return instance;
            }
            return method.invoke(instance, args);
        }
    }

    @SuppressWarnings("unchecked")
    <T> T mockCCLProxy(T originalObject, Class<T> proxyInterface) {
        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] {proxyInterface},
                new ContextClassLoaderSettingInvocationHandler(originalObject));
    }

    @SuppressWarnings("unchecked")
    <T> T mockHtmlSafeProxy(T originalObject, Class<T> proxyInterface) throws ReflectiveOperationException {
        var annotatedValue = new AnnotatedValue<>(originalObject, emptyList());

        var constructor = HTML_SAFE_HANDLER_CLASS.getDeclaredConstructor(AnnotationBoxedElement.class, Set.class);
        constructor.setAccessible(true);

        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] {proxyInterface},
                constructor.newInstance(
                        annotatedValue, Set.of(originalObject.getClass().getMethods()[0])));
    }

    @SuppressWarnings("unchecked")
    <T> T mockOsgiComponentProxy(T originalObject, Class<T> proxyInterface) throws Exception {
        var mockBundle = mock(BundleContext.class);
        ServiceReference<?>[] serviceReferences = new ServiceReference[] {mock(ServiceReference.class)};
        when(mockBundle.getServiceReferences(nullable(String.class), anyString()))
                .thenReturn(serviceReferences);
        when(mockBundle.getService(any())).thenReturn(originalObject);

        var constructor = OSGI_COMPONENT_HANDLER_CLASS.getDeclaredConstructor(BundleContext.class, String.class);
        constructor.setAccessible(true);

        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] {proxyInterface},
                constructor.newInstance(mockBundle, ""));
    }

    <T> T mockSpringOsgiService(T originalObject, Class<T> proxyInterface) {
        return mockSpringInfraProxy(mockCCLProxy(originalObject, proxyInterface), proxyInterface);
    }
}
