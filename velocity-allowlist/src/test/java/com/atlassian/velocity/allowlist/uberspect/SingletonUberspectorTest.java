package com.atlassian.velocity.allowlist.uberspect;

import org.apache.velocity.runtime.RuntimeLogger;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.introspection.Introspector;
import org.apache.velocity.util.introspection.Uberspect;
import org.apache.velocity.util.introspection.UberspectImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class SingletonUberspectorTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private RuntimeServices mockRuntimeServices;

    @Mock
    private Log mockLog;

    @Mock
    private RuntimeLogger mockRuntimeLogger;

    @Mock
    private UberspectImpl mockUberspect;

    @Mock
    private Introspector mockIntrospector;

    private DelegateMockedSingletonUberspector singletonUberspector;
    private DelegateMockedSingletonUberspector altSingletonUberspector;

    class DelegateMockedSingletonUberspector extends SingletonUberspector {
        @Override
        protected Uberspect newUberspector() {
            return mockUberspect;
        }

        @Override
        protected Introspector newIntrospector(Log log, RuntimeServices runtimeServices) {
            return mockIntrospector;
        }
    }

    @Before
    public void setUp() {
        singletonUberspector = new DelegateMockedSingletonUberspector();
        altSingletonUberspector = new DelegateMockedSingletonUberspector();
    }

    // The class under test relies on unresettable static state so tests must run in a specific order all at once
    @Test
    public void testAll() throws Exception {
        singletonUberspector.setRuntimeServices(mockRuntimeServices);
        altSingletonUberspector.setLog(mockLog);
        singletonUberspector.setRuntimeLogger(mockRuntimeLogger);

        // Init state settable before init()
        verify(mockUberspect, times(1)).setLog(any());
        verify(mockUberspect, times(1)).setRuntimeServices(any());

        // Deprecated method no-op
        verify(mockUberspect, never()).setRuntimeLogger(any());

        reset(mockUberspect);

        singletonUberspector.init();
        singletonUberspector.init();
        altSingletonUberspector.init();
        altSingletonUberspector.init();

        // Can only init() once
        verify(mockUberspect, times(1)).init();

        singletonUberspector.setRuntimeServices(mockRuntimeServices);
        singletonUberspector.setLog(mockLog);

        // Init state not settable after init()
        verify(mockUberspect, never()).setLog(any());
        verify(mockUberspect, never()).setRuntimeServices(any());

        Object obj = new Object();
        String methodName = "someMethod";
        Object[] args = {};

        singletonUberspector.getMethod(obj, methodName, args, null);
        altSingletonUberspector.getMethod(obj, methodName, args, null);

        // Both instances delegate to the same Uberspect instance
        verify(mockUberspect, times(2)).getMethod(obj, methodName, args, null);

        singletonUberspector.getIntrospectorInternal(mockLog, mockRuntimeServices);
        singletonUberspector.getIntrospectorInternal(mockLog, mockRuntimeServices);
        altSingletonUberspector.getIntrospectorInternal(mockLog, mockRuntimeServices);
        altSingletonUberspector.getIntrospectorInternal(mockLog, mockRuntimeServices);

        // Both instances delegate to the same Introspector instance
        assertEquals(mockIntrospector, singletonUberspector.getIntrospector());
        assertEquals(mockIntrospector, altSingletonUberspector.getIntrospector());
    }
}
