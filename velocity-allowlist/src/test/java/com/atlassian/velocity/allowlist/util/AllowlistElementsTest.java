package com.atlassian.velocity.allowlist.util;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.module.Element;

import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThrows;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AllowlistElementsTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock(answer = RETURNS_DEEP_STUBS)
    private Element element;

    private AllowlistElements allowlistElements;

    @Before
    public void setUp() {
        when(element.elements(anyString())).thenReturn(emptyList());
    }

    private static Element mockElement(String text) {
        Element element = mock(Element.class);
        when(element.getText()).thenReturn(text);
        return element;
    }

    @Test
    public void getClasses() {
        Element mockEl1 = mockElement(" " + Object.class.getName());
        Element mockEl2 = mockElement(String.class.getName() + " ");
        when(element.elements("class")).thenReturn(List.of(mockEl1, mockEl2));

        allowlistElements = AllowlistElements.parse(element);

        assertThat(allowlistElements.getClasses(), containsInAnyOrder(Object.class.getName(), String.class.getName()));
    }

    @Test
    public void getPackages() {
        Element mockEl1 = mockElement(" " + Object.class.getPackageName());
        Element mockEl2 = mockElement(Set.class.getPackageName() + " ");
        when(element.elements("package")).thenReturn(List.of(mockEl1, mockEl2));

        allowlistElements = AllowlistElements.parse(element);

        assertThat(
                allowlistElements.getPackages(),
                containsInAnyOrder(Object.class.getPackageName(), Set.class.getPackageName()));
    }

    @Test
    public void getMethods() {
        Element mockEl1 = mockElement(Object.class.getName() + "#toString()");
        Element mockEl2 = mockElement(Object.class.getName() + "#hashCode()");
        Element mockEl3 = mockElement(Set.class.getName() + "#size()");
        Element mockEl4 = mockElement(Set.class.getName() + "#remove(java.lang.Object)");
        Element mockEl5 = mockElement(Set.class.getName() + "#addAll(java.util.Collection)");
        Element mockEl6 = mockElement(
                Optional.class.getName() + "#ifPresentOrElse(java.util.function.Consumer java.lang.Runnable)");

        when(element.elements("method")).thenReturn(List.of(mockEl1, mockEl2, mockEl3, mockEl4, mockEl5, mockEl6));

        allowlistElements = AllowlistElements.parse(element);

        var expectedMap = Map.of(
                Object.class.getName(), Set.of("toString()", "hashCode()"),
                Set.class.getName(), Set.of("size()", "remove(java.lang.Object)", "addAll(java.util.Collection)"),
                Optional.class.getName(), Set.of("ifPresentOrElse(java.util.function.Consumer java.lang.Runnable)"));

        assertThat(allowlistElements.getMethods().keySet(), everyItem(is(in(expectedMap.keySet()))));
        expectedMap.forEach((key, value) -> assertThat(allowlistElements.getMethods(), hasEntry(is(key), is(value))));
    }

    @Test
    public void getMethods_invalid() {
        Element mockEl1 = mockElement(Object.class.getName() + "#toString()#invalid()");
        Element mockEl2 = mockElement(Object.class.getName() + "toString");

        when(element.elements("method")).thenReturn(List.of(mockEl1));

        assertThrows(PluginParseException.class, () -> AllowlistElements.parse(element));

        when(element.elements("method")).thenReturn(List.of(mockEl2));

        assertThrows(PluginParseException.class, () -> AllowlistElements.parse(element));
    }
}
