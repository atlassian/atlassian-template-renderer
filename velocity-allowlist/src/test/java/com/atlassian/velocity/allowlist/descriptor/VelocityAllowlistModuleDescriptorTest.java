package com.atlassian.velocity.allowlist.descriptor;

import java.util.Map;
import java.util.Set;

import org.dom4j.DocumentException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.internal.module.Dom4jDelegatingElement;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.velocity.allowlist.api.internal.PluginAllowlist;

import static java.util.Collections.emptyList;
import static org.dom4j.DocumentHelper.parseText;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VelocityAllowlistModuleDescriptorTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ModuleFactory moduleFactory;

    @Mock
    private PluginAllowlist pluginAllowlist;

    @InjectMocks
    private VelocityAllowlistModuleDescriptor descriptor;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private Plugin mockedPlugin;

    @Mock(answer = RETURNS_DEEP_STUBS)
    private Element mockedElement;

    @Before
    public void setUp() throws Exception {
        when(mockedElement.attributeValue("key")).thenReturn("key");
        when(mockedElement.element("java-version").attributeValue("min")).thenReturn("11");
        when(mockedElement.elements(any())).thenReturn(emptyList());
    }

    private Element parseXml(String xml) throws DocumentException {
        return new Dom4jDelegatingElement(parseText(xml).getRootElement());
    }

    @Test
    public void getModule() {
        assertNull(descriptor.getModule());
    }

    @Test
    public void init() {
        descriptor.init(mockedPlugin, mockedElement);

        assertNotNull(descriptor.getAllowlistElements());
    }

    @Test
    public void enabled() {
        // Given
        descriptor.init(mockedPlugin, mockedElement);

        // When
        descriptor.enabled();

        // Then
        assertTrue(descriptor.isEnabled());
        verify(pluginAllowlist).reload();
    }

    /**
     * This ordinarily cannot happen but Confluence has previously come across unusual bugs where plugins that utilise
     * pocketknife exhibit this incorrect lifecycle behaviour. It is best we throw ASAP in such cases.
     */
    @Test
    public void enabled_withoutInit() {
        assertThrows(NullPointerException.class, () -> descriptor.enabled());
    }

    @Test
    public void disabled() {
        // Given
        descriptor.init(mockedPlugin, mockedElement);
        descriptor.enabled();
        clearInvocations(pluginAllowlist);

        // When
        descriptor.disabled();

        // Then
        assertFalse(descriptor.isEnabled());
        verify(pluginAllowlist).reload();
    }

    @Test
    public void parse_class() throws Exception {
        descriptor.init(
                mockedPlugin,
                parseXml(
                        """
                <velocity-allowlist key="key">
                    <class>className</class>
                </velocity-allowlist>
                """));

        assertEquals(
                "className",
                descriptor.getAllowlistElements().getClasses().iterator().next());
    }

    @Test
    public void parse_package() throws Exception {
        descriptor.init(
                mockedPlugin,
                parseXml(
                        """
                <velocity-allowlist key="key">
                    <package>packageName</package>
                </velocity-allowlist>
                """));

        assertEquals(
                "packageName",
                descriptor.getAllowlistElements().getPackages().iterator().next());
    }

    @Test
    public void parse_method() throws Exception {
        descriptor.init(
                mockedPlugin,
                parseXml(
                        """
                <velocity-allowlist key="key">
                    <method>package.Class#method()</method>
                    <method>package.Class#methodB()</method>
                    <method>package.Class2#methodC()</method>
                </velocity-allowlist>
                """));

        assertThat(
                descriptor.getAllowlistElements().getMethods(),
                is(Map.of(
                        "package.Class", Set.of("method()", "methodB()"),
                        "package.Class2", Set.of("methodC()"))));
    }

    @Test
    public void parse_method_invalid() {
        assertThrows(
                PluginParseException.class,
                () -> descriptor.init(
                        mockedPlugin,
                        parseXml(
                                """
                <velocity-allowlist key="key">
                    <method>package.Class!method()</method>
                </velocity-allowlist>
                """)));
    }
}
