package com.atlassian.templaterenderer.velocity.one.six.internal.spring;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.templaterenderer.TemplateContextFactory;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.templaterenderer.TemplateRendererFactory;
import com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer;
import com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRendererFactory;
import com.atlassian.templaterenderer.velocity.one.six.internal.VelocityTemplateRendererFactoryServiceFactory;
import com.atlassian.templaterenderer.velocity.one.six.internal.VelocityTemplateRendererServiceFactory;

import static com.atlassian.plugins.osgi.javaconfig.ExportOptions.as;
import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.exportOsgiService;

@Import(OsgiServiceImports.class)
@Configuration
public class OsgiServiceExports {

    @Bean
    public FactoryBean<ServiceRegistration> exportTemplateRenderer(
            EventPublisher eventPublisher, TemplateContextFactory templateContextFactory) {
        return exportOsgiService(
                new VelocityTemplateRendererServiceFactory(eventPublisher, templateContextFactory),
                as(TemplateRenderer.class, VelocityTemplateRenderer.class).withProperty("engine", "velocity-1.6"));
    }

    @Bean
    public FactoryBean<ServiceRegistration> exportTemplateRendererFactory(
            EventPublisher eventPublisher, TemplateContextFactory templateContextFactory) {
        return exportOsgiService(
                new VelocityTemplateRendererFactoryServiceFactory(templateContextFactory, eventPublisher),
                as(TemplateRendererFactory.class, VelocityTemplateRendererFactory.class)
                        .withProperty("engine", "velocity-1.6"));
    }
}
