package com.atlassian.templaterenderer.velocity.one.six.internal.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.templaterenderer.velocity.CachingWebPanelRendererTracker;

@Configuration
public class LocalBeans {

    @Bean
    public CachingWebPanelRendererTracker cachingWebPanelRendererTracker() {
        return new CachingWebPanelRendererTracker();
    }
}
