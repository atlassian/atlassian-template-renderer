package com.atlassian.templaterenderer.velocity.one.six.internal;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ExtendedProperties;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.app.event.EventHandler;
import org.apache.velocity.runtime.log.CommonsLogLogChute;
import org.apache.velocity.util.introspection.UberspectImpl;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.templaterenderer.RenderingException;
import com.atlassian.templaterenderer.TemplateContextFactory;
import com.atlassian.templaterenderer.velocity.CompositeClassLoader;
import com.atlassian.templaterenderer.velocity.TemplateRendererAnnotationBoxingUberspect;
import com.atlassian.templaterenderer.velocity.TemplateRendererHtmlAnnotationEscaper;
import com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer;
import com.atlassian.templaterenderer.velocity.resource.AtlassianClasspathResourceLoader;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.velocity.htmlsafe.HtmlSafeDirective;
import com.atlassian.velocity.htmlsafe.directive.DisableHtmlEscaping;
import com.atlassian.velocity.htmlsafe.directive.EnableHtmlEscaping;
import com.atlassian.velocity.htmlsafe.event.referenceinsertion.DisableHtmlEscapingDirectiveHandler;
import com.atlassian.velocity.htmlsafe.introspection.AnnotationBoxingUberspect;

import static com.atlassian.util.profiling.Metrics.metric;

/**
 * A velocity template renderer
 */
public class VelocityTemplateRendererImpl implements VelocityTemplateRenderer {
    public static final String DEFAULT_ENCODING = "UTF-8";
    private final EventPublisher eventPublisher;
    private final ClassLoader classLoader;
    private final String pluginKey;
    private final TemplateContextFactory templateContextFactory;

    @VisibleForTesting
    final VelocityEngine velocity;

    public VelocityTemplateRendererImpl(
            ClassLoader classLoader,
            EventPublisher eventPublisher,
            String pluginKey,
            Map<String, String> properties,
            TemplateContextFactory templateContextFactory) {
        this.classLoader = classLoader;
        this.eventPublisher = eventPublisher;
        this.pluginKey = pluginKey;
        this.templateContextFactory = templateContextFactory;

        final ClassLoader originalClassLoader = Thread.currentThread().getContextClassLoader();
        // Don't use the context class loader here since it is a OSGIBundleDelegatingClassLoader that actually uses the
        // originating bundle. Essentially the same as the classLoader passed in.  Using this.getClass.getClassLoader()
        // uses *this* bundles classloader meaning the right version (the version this bundle depends on) of velocity
        // will be loaded.
        // Velocity-HtmlSafe lives in another OSGi classloader again, so we need to ensure its classes are on the
        // classpath of the composite loader too
        final CompositeClassLoader compositeClassLoader = new CompositeClassLoader(
                this.getClass().getClassLoader(), AnnotationBoxingUberspect.class.getClassLoader(), classLoader);

        Thread.currentThread().setContextClassLoader(compositeClassLoader);
        try {
            // Instantiating the VelocityEngine loads some classes, like IntrospectorCacheImpl, so even the
            // constructor call needs to be inside the block with the CompositeClassLoader applied
            velocity = new VelocityEngine();

            try (InputStream productVelocityPropertiesStream =
                    compositeClassLoader.getResourceAsStream("velocity.properties")) {
                if (productVelocityPropertiesStream != null) {
                    ExtendedProperties productVelocityProperties = new ExtendedProperties();
                    productVelocityProperties.load(productVelocityPropertiesStream);
                    velocity.setExtendedProperties(productVelocityProperties);
                }
            }

            Object value = velocity.getProperty(Velocity.UBERSPECT_CLASSNAME);
            if (value == null || value.equals(UberspectImpl.class.getName())) {
                // If unconfigured or unchanged from default, override it
                velocity.setProperty(
                        Velocity.UBERSPECT_CLASSNAME, TemplateRendererAnnotationBoxingUberspect.class.getName());
            }

            velocity.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM_CLASS, CommonsLogLogChute.class.getName());
            velocity.setProperty(Velocity.RESOURCE_LOADER, "classpath");
            velocity.setProperty("classpath.resource.loader.class", AtlassianClasspathResourceLoader.class.getName());

            // Disable these allowlists for plugins, they can use whatever they want in development mode
            // we should only be ever loading files from the plugin given we're using
            // com.atlassian.templaterenderer.BundleClassLoaderAccessor
            velocity.setProperty(Velocity.RESOURCE_FILE_ALLOWLIST_ENABLE, "false");
            velocity.setProperty(Velocity.RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "false");

            velocity.addProperty("userdirective", EnableHtmlEscaping.class.getName());
            velocity.addProperty("userdirective", DisableHtmlEscaping.class.getName());
            velocity.addProperty("userdirective", HtmlSafeDirective.class.getName());
            for (Map.Entry<String, String> prop : properties.entrySet()) {
                velocity.setProperty(prop.getKey(), prop.getValue());
            }
            handleCache();
            velocity.clearProperty("velocimacro.library");
            velocity.init();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            Thread.currentThread().setContextClassLoader(originalClassLoader);
        }
    }

    @Override
    public void render(String templateName, Writer writer) throws RenderingException, IOException {
        render(templateName, Collections.emptyMap(), writer);
    }

    @Override
    public void render(String templateName, Map<String, Object> context, Writer writer)
            throws RenderingException, IOException {

        ClassLoader originalClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(classLoader);
        try (Ticker ignored = metric("webTemplateRenderer")
                .tag("templateRenderer", "velocity")
                .tag("templateName", templateName)
                .fromPluginKey(pluginKey)
                .withAnalytics()
                .startTimer()) {

            // This has the risk of creating invalid HTML or being used for something else, we're relying on convention
            // this is safe enough for development (around whole templates, not fragments), when not on by default.
            boolean shouldWriteComments =
                    Boolean.getBoolean("atl.html.trace.comments.velocity-template-renderer.enabled");

            if (shouldWriteComments) {
                writer.write(String.format(
                        "<!-- [START] [velocity-template-renderer] plugin-key: %s template: %s -->",
                        pluginKey, templateName));
            }
            Template template = velocity.getTemplate(templateName, DEFAULT_ENCODING);
            template.merge(createContext(context), writer);
            if (shouldWriteComments) {
                writer.write(String.format(
                        "<!-- [END] [velocity-template-renderer] plugin-key: %s template: %s -->",
                        pluginKey, templateName));
            }

            writer.flush();
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw new RenderingException(e);
        } finally {
            Thread.currentThread().setContextClassLoader(originalClassLoader);
        }
    }

    @Override
    public String renderFragment(String fragment, Map<String, Object> context) {
        try {
            StringWriter tempWriter = new StringWriter(fragment.length());
            velocity.evaluate(createContext(context), tempWriter, "renderFragment", fragment);
            return tempWriter.toString();
        } catch (Exception e) {
            throw new RenderingException(e);
        }
    }

    private void handleCache() {
        final String IS_DEV_MODE = Boolean.toString(isDevMode());
        final String IS_PROD_MODE = Boolean.toString(!isDevMode());

        velocity.setProperty("classpath.resource.loader.cache", IS_PROD_MODE);
        velocity.setProperty("plugin.resource.loader.cache", IS_PROD_MODE);
        velocity.setProperty("velocimacro.library.autoreload", IS_DEV_MODE);
    }

    private boolean isDevMode() {
        return Boolean.getBoolean("atlassian.dev.mode");
    }

    private VelocityContext createContext(Map<String, Object> contextParams) {
        var velocityContext = new VelocityContext(templateContextFactory.createContext(pluginKey, contextParams));
        velocityContext.attachEventCartridge(createCartridgeFrom(
                List.of(new DisableHtmlEscapingDirectiveHandler(new TemplateRendererHtmlAnnotationEscaper()))));
        return velocityContext;
    }

    private EventCartridge createCartridgeFrom(final List<? extends EventHandler> eventHandlers) {
        final EventCartridge cartridge = new EventCartridge();
        for (final EventHandler eventHandler : eventHandlers) {
            cartridge.addEventHandler(eventHandler);
        }
        return cartridge;
    }

    /**
     * Check whether the given template exists or not
     */
    @Override
    public boolean resolve(String templateName) {
        return classLoader.getResource(templateName) != null;
    }
}
