package com.atlassian.templaterenderer.velocity.one.six.internal;

import java.util.Collections;

import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.osgi.util.BundleClassLoaderAccessor;
import com.atlassian.plugin.util.resource.AlternativeDirectoryResourceLoader;
import com.atlassian.templaterenderer.TemplateContextFactory;

/**
 * Service factory for instantiating a template renderer for the given bundle
 */
public class VelocityTemplateRendererServiceFactory implements ServiceFactory {
    /**
     * This can be replaced with OsgiPlugin.ATLASSIAN_PLUGIN_KEY once we update to the latest version of plugins
     */
    private static final String ATLASSIAN_PLUGIN_KEY = "Atlassian-Plugin-Key";

    private final EventPublisher eventPublisher;
    private final TemplateContextFactory templateContextFactory;

    public VelocityTemplateRendererServiceFactory(
            EventPublisher eventPublisher, TemplateContextFactory templateContextFactory) {
        this.eventPublisher = eventPublisher;
        this.templateContextFactory = templateContextFactory;
    }

    public Object getService(Bundle bundle, ServiceRegistration serviceRegistration) {
        String pluginKey = (String) bundle.getHeaders().get(ATLASSIAN_PLUGIN_KEY);
        // We want velocity to use the callers classloader
        ClassLoader bundleClassLoader =
                BundleClassLoaderAccessor.getClassLoader(bundle, new AlternativeDirectoryResourceLoader());
        return new VelocityTemplateRendererImpl(
                bundleClassLoader, eventPublisher, pluginKey, Collections.emptyMap(), templateContextFactory);
    }

    public void ungetService(Bundle bundle, ServiceRegistration serviceRegistration, Object service) {
        // No state is stored, so nothing to do
    }
}
