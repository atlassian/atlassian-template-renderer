package com.atlassian.templaterenderer.velocity.one.six;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.templaterenderer.TemplateContextFactory;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.templaterenderer.velocity.AbstractCachingWebPanelRenderer;
import com.atlassian.templaterenderer.velocity.one.six.internal.VelocityTemplateRendererImpl;

import static java.util.Collections.emptyMap;

/**
 * @since 2.5.0
 */
public class VelocityWebPanelRenderer extends AbstractCachingWebPanelRenderer {
    private final EventPublisher eventPublisher;
    private final TemplateContextFactory templateContextFactory;

    public VelocityWebPanelRenderer(
            EventPublisher eventPublisher,
            TemplateContextFactory templateContextFactory,
            PluginEventManager pluginEventManager) {
        super(pluginEventManager);
        this.eventPublisher = eventPublisher;
        this.templateContextFactory = templateContextFactory;
    }

    @Override
    protected TemplateRenderer createRenderer(Plugin plugin) {
        return new VelocityTemplateRendererImpl(
                plugin.getClassLoader(), eventPublisher, plugin.getKey(), emptyMap(), templateContextFactory);
    }
}
