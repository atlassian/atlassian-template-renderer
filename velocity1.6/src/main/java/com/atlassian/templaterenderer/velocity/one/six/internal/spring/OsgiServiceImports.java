package com.atlassian.templaterenderer.velocity.one.six.internal.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.templaterenderer.TemplateContextFactory;

import static com.atlassian.plugins.osgi.javaconfig.OsgiServices.importOsgiService;

@Configuration
public class OsgiServiceImports {
    @Bean
    public EventPublisher eventPublisher() {
        return importOsgiService(EventPublisher.class);
    }

    @Bean
    public PluginEventManager pluginEventManager() {
        return importOsgiService(PluginEventManager.class);
    }

    @Bean
    public TemplateContextFactory templateContextFactory() {
        return importOsgiService(TemplateContextFactory.class);
    }
}
