package com.atlassian.templaterenderer.velocity.one.six.internal;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.templaterenderer.TemplateContextFactory;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.templaterenderer.velocity.resource.AtlassianClasspathResourceLoader;
import com.atlassian.templaterenderer.velocity.tests.AbstractVelocityRendererImplTest;

import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILETYPE_ALLOWLIST_ENABLE;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILE_ALLOWLIST_ENABLE;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_LOADER;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class VelocityTemplateRendererImplTest extends AbstractVelocityRendererImplTest {
    @Rule
    public final ClearSystemPropertyRule systemPropertyRule = new ClearSystemPropertyRule();

    @Rule
    public final ReplaceFileContentRule replaceFileContentRule = new ReplaceFileContentRule();

    @Override
    protected TemplateRenderer createVelocityRenderer(
            TemplateContextFactory contextFactory,
            ClassLoader classLoader,
            String pluginKey,
            Map<String, String> initProperties) {
        EventPublisher eventPublisher = mock(EventPublisher.class);
        return new VelocityTemplateRendererImpl(classLoader, eventPublisher, pluginKey, initProperties, contextFactory);
    }

    private VelocityTemplateRendererImpl createVelocityRenderer() {
        return new VelocityTemplateRendererImpl(
                mock(ClassLoader.class),
                mock(EventPublisher.class),
                "plugin.key",
                Map.of(),
                mock(TemplateContextFactory.class));
    }

    @Test
    public void shouldCacheTemplates() throws IOException, URISyntaxException {
        assertThat(renderTemplate(VM_RELOADING_TEST), equalTo("before change"));

        replaceFileContentRule.replaceFileContent(VM_RELOADING_TEST, "after change");
        assertThat(renderTemplate(VM_RELOADING_TEST), equalTo("before change"));
    }

    @Test
    public void shouldCacheMacroLibs() throws IOException, URISyntaxException {
        assertThat(renderTemplate(VM_MACRO_TEST), equalTo("MyMacro content"));

        replaceFileContentRule.replaceFileContent(VM_GLOBAL_LIB, "#macro (myMacro)changed#end");
        assertThat(renderTemplate(VM_MACRO_TEST), equalTo("MyMacro content"));
    }

    @Test
    public void shouldAllowReloadingTemplatesInDevMode() throws IOException, URISyntaxException {
        systemPropertyRule.setProperty("atlassian.dev.mode", "true");
        initRenderer();

        assertThat(renderTemplate(VM_RELOADING_TEST), equalTo("before change"));

        replaceFileContentRule.replaceFileContent(VM_RELOADING_TEST, "after change");
        assertThat(renderTemplate(VM_RELOADING_TEST), equalTo("after change"));
    }

    @Test
    public void shouldAllowReloadingMacroLibsInDevMode() throws IOException, URISyntaxException {
        systemPropertyRule.setProperty("atlassian.dev.mode", "true");
        initRenderer();

        assertThat(renderTemplate(VM_MACRO_TEST), equalTo("MyMacro content"));

        replaceFileContentRule.replaceFileContent(VM_GLOBAL_LIB, "#macro (myMacro)changed#end");
        assertThat(renderTemplate(VM_MACRO_TEST), equalTo("changed"));
    }

    /**
     * We should only load files that exist in the plugin bundles, otherwise that would defeat the hard work we've done
     * creating an allowlist for template files directly on the filesystem
     * <p>
     * Here we're testing the first part of the chain
     * <ol>
     *     <li><b>We use a specific classpath resource loader</b></li>
     *     <li>This resource loader can only access the files in the plugin</li>
     * </ol>
     * The rest is tested in {@code NoAccessToWebappFolderTemplatesWiredTest
     * #testTemplateFilesInTheWebapp_shouldNotBeRenderedByAtlassianTemplateRenderer()}
     * </p>
     */
    @Test
    public void shouldUseASetOfTrustedResourceLoaders() {
        var velocityTemplateRenderer = createVelocityRenderer();
        // It only ever uses our trusted class loader
        assertEquals(List.of("classpath"), velocityTemplateRenderer.velocity.getProperty(RESOURCE_LOADER));
        assertEquals(
                AtlassianClasspathResourceLoader.class.getCanonicalName(),
                velocityTemplateRenderer.velocity.getProperty("classpath.resource.loader.class"));
    }

    @Test
    public void testVelocityEngine_shouldHaveTheFileAllowlistDisabled_soThatPluginsAreNotAffected() {
        var velocityTemplateRenderer = createVelocityRenderer();

        assertFalse((Boolean) velocityTemplateRenderer.velocity.getProperty(RESOURCE_FILE_ALLOWLIST_ENABLE));
    }

    @Test
    public void testVelocityEngine_shouldHaveTheFileTypeAllowlistDisabled_soThatPluginsAreNotAffected() {
        var velocityTemplateRenderer = createVelocityRenderer();

        assertFalse((Boolean) velocityTemplateRenderer.velocity.getProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE));
    }
}
