package com.atlassian.templaterenderer.velocity.one.six.internal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

/**
 * Replaces file content and restores the original one when finished.
 */
public class ReplaceFileContentRule extends TestWatcher {
    private final Map<String, String> originalFiles = new HashMap<>();

    public void replaceFileContent(String filename, String newContent) throws URISyntaxException, IOException {
        if (!originalFiles.containsKey(filename)) {
            originalFiles.put(filename, readFile(filename));
        }

        writeFile(filename, newContent);
    }

    @Override
    protected void finished(final Description description) {
        try {
            for (Map.Entry<String, String> entry : originalFiles.entrySet()) {
                writeFile(entry.getKey(), entry.getValue());
            }
        } catch (URISyntaxException | FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeFile(String filename, String newContent) throws URISyntaxException, FileNotFoundException {
        try (PrintWriter outputStream =
                new PrintWriter(new File(getClass().getResource("/" + filename).toURI()))) {
            outputStream.append(newContent);
            outputStream.flush();
        }
    }

    private String readFile(String filename) throws IOException {
        try (InputStream is = getClass().getResourceAsStream("/" + filename)) {
            return new Scanner(is, "UTF-8").useDelimiter("\\A").next();
        }
    }
}
