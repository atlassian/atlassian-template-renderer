package com.atlassian.templaterenderer.velocity.one.six.internal;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PomPropertiesInSyncTest {

    @Test
    public void testVelocityVersionsAreInSync() {
        String velocity16Version = System.getProperty("velocity16.version");
        String forkVersion = System.getProperty("velocity16.fork.version");

        // Check if the fork version starts with the velocity16.version and has the expected postfix
        assertTrue(forkVersion.matches(velocity16Version + "-atlassian-\\d+"));
    }
}
